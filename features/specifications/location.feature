#language: pt
#encoding: utf-8

@FullSmoke
@Location

Funcionalidade: Location

  - Narrativa:
  Possibilitar que a localização atual do usuário seja identificada


  Cenario: Validar localização atual
    Dado que acesso o app --onboarding
    E pulo o onboarding --onboarding
    Quando uso minha localização atual --location
    Entao sou direcionado para a tela inicial com meu endereço preenchido --home
    E clico para alterar o endereço --home
    E escolho um endereço "random" --location
    E sou direcionado para a tela inicial com meu endereço preenchido --home
