#language: pt
#encoding: utf-8

@FullSmoke
@Search

Funcionalidade: Search

  - Narrativa:
  Eu como cliente preciso poder efetuar buscas a fim de encontrar o melhor anfitrião para minha necessidade

  Cenário: Validar tela busca
    Dado estou na tela inicial com uma localização informada --home
      | address |
      | random  |
    Quando clico na opção de busca por anfitrião --home
    Entao valido que estou na tela de busca --search
    E verifico que os resultados da busca foram retornados com sucesso --search
    E que o conteúdo das informações não está vazio --search
    E valido a apresentação de mais resultados --search
    E verifico que os resultados da busca foram retornados com sucesso --search

  Cenário: Validar filtros adicionais
    Dado estou na tela de busca com uma localização informada --search
      | address |
      | random  |
    Quando seleciono filtros adicionais --search
    Entao verifico que os resultados da busca foram retornados com sucesso --search

  Cenário: Validar alteração de endereço
    Dado estou na tela de busca com uma localização informada --search
      | address |
      | random  |
    Quando clico para alterar o endereço --search
      | address |
      | random  |
    Entao verifico que os resultados da busca foram retornados com sucesso --search

  Cenário: Validar apresentação do mapa
    Dado estou na tela de busca com uma localização informada --search
      | address |
      | random  |
    E altero a data de busca --search
    E verifico que os resultados da busca foram retornados com sucesso --search
    Quando abro o mapa do google --search
    Entao o mapa é apresentado corretamente --search
