#language: pt
#encoding: utf-8

@FullSmoke
@Conversation
Funcionalidade: Conversation

  - Narrativa:
  Possibilitar o envio de mensagem inicial ao anfitrião afim de fechar futuras reservas

  Esquema do Cenario: Verificar quantidade de noites e valores cobrados
    Dado estou na tela de conversa com um anfitrião de testes e serviço de boarding --conversation
      | address | email  | password |
      | random  | client | default  |
    E altero a data de hospedagem --conversation
    Quando escolho o período de checkin para "<checkin>" e o período de checkout para "<checkout>" --conversation
    E seleciono a opção de pré-encontro "random" --conversation
    Então verifico se meio período está sendo apresentado corretamente --conversation
    E verifico se a quantidade de noites apresentada está conforme o que foi escolhido --conversation
    E eu envio uma mensagem "random" --conversation
    E uma tela de sucesso é apresentada --conversation

    Exemplos:
      | checkin | checkout |
      | manhã   | noite    |

  Cenario: Validar novo pet adicionado
    Dado estou na tela de conversa com um anfitrião de testes e serviço de boarding --conversation
      | address | email  | password |
      | random  | client | default  |
    E altero a data de hospedagem --conversation
    Quando adiciono um novo pet --conversation
    Então o novo pet é apresentado com sucesso --conversation
    E desabilito um pet existente --conversation
    E o valor total é alterado com sucesso --conversation
    E habilito um pet existente --conversation
    E o valor total é alterado com sucesso --conversation
