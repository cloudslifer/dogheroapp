#language: pt
#encoding: utf-8

@FullSmoke
@Home

Funcionalidade: Home

  - Narrativa:
  Acessar a homepage para garantir que as funcionalidades principais são apresentadas


  Cenário: Validar home page - usuários logados e não logados
    Dado que acesso o app --onboarding
    Quando pulo o onboarding e utilizo uma localidade desejada --onboarding
      | address |
      | random  |
    Então verifico que estou na tela inicial --home
    E verifico os textos apresentados --home
    E que as funcionalidades principais são apresentadas --home
    E que o menu inferior é apresentado corretamente para usuários não logados --home
    E faço o login com meu email "client" e senha "default" --login
    E verifico que estou na tela inicial --home
    E verifico os textos apresentados --home
    E que as funcionalidades principais são apresentadas --home
    E que o menu inferior é apresentado corretamente para usuários logados --home
