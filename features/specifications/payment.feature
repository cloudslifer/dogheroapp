#language: pt
#encoding: utf-8

@FullSmoke
@Payment
Funcionalidade: Payment

  - Narrativa:
  Possibilitar o pagamento das reservas

  @thus
  Cenario: Efetuar reserva com boleto
    Dado estou na tela de pagamento com um anfitrião de testes e serviço de boarding --payment-boarding
      | address | email  | password |
      | random  | client | default  |
    E altero meu nome para "random", cpf para "random" e celular para "random" --payment-boarding
    E seleciono um endereço aleatório --payment-boarding
    Quando seleciono o pagamento com boleto --payment-boarding
    E finalizo o meu pagamento --payment-boarding
    Então verifico que minha reserva foi solicitada com sucesso --payment-boarding

  Cenario: Efetuar reserva com cartão de crédito
    Dado estou na tela de pagamento com um anfitrião de testes e serviço de boarding --payment-boarding
      | address | email  | password |
      | random  | client | default  |
    E seleciono um cartão aleatório --payment-boarding
    Quando seleciono o pagamento com cartão de crédito --payment-boarding
    E seleciono meu parcelamento --payment-boarding
    E finalizo o meu pagamento --payment-boarding
    Então verifico que minha reserva foi solicitada com sucesso --payment-boarding


