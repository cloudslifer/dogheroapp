#language: pt
#encoding: utf-8

@FullSmoke
@MyPets

Funcionalidade: My Pets

  - Narrativa:
  Adicionar pets a fim de poder escolher entre eles na hora de efetuar reservas

  Cenário: Validar home page - usuário não logado
    Dado eu estou na tela de 'Meus Pets' com uma localização informada e logado --my-pets
      | address | email  | password |
      | random  | client | default  |
    Quando eu cadastro um pet com nome "random" sexo "random" raça "random" idade "random" tamanho "random" foto "yes" --my-pets
    Então meu pet é adicionado com sucesso --my-pets

