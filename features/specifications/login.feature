#language: pt
#encoding: utf-8

@FullSmoke
@Login

Funcionalidade: Login

  - Narrativa:
  Fazer login para garantir que apenas usuários válidos acessem seus dados pessoais no sistema

  Contexto: Acesso a home
    Dado estou na tela inicial com uma localização informada --home
      | address |
      | random  |

  Cenário: Validar login com usuário existente
    Quando faço o login com meu email "client" e senha "default" --login
    Então o meu acesso é efetuado com sucesso --menu
    E efetuo o logout

#  Esquema do Cenário: Validar que login não é efetuado com sucesso com usuários inválidos
#    Quando faço o login com meu email "<email>" e senha "<senha>" --login
#    Então a mensagem de dados incorretos é apresentada --login

#    Exemplos:
#      | email               | senha          |
#      | abc@mailinator.com  | 12345ABC       |




