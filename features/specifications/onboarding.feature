#language: pt
#encoding: utf-8

@FullSmoke
@Onboarding
Funcionalidade: onboarding

  - Narrativa:
  Possibilitar interação no primeiro acesso do usuário

  Cenario: Cadastrar pet no onboarding
    Dado que acesso o app --onboarding
    Quando eu cadastro inicialmente um pet com nome "random" sexo "random" raça "random" idade "random" tamanho "random" foto "random" --my-pets
    Então uma mensagem de sucesso é apresentada solicitando um cadastro na doghero --my-pets
