#language: pt
#encoding: utf-8

@FullSmoke
@Reservation
Funcionalidade: Reservation

  - Narrativa:
  Possibilitar a solicitação de reservas

  Esquema do Cenario: Verificar quantidade de noites e valores cobrados
    Dado estou na tela de reserva com um anfitrião de testes e serviço de boarding --reservation
      | address | email  | password |
      | random  | client | default  |
    E altero a data de hospedagem --reservation
    Quando escolho o período de checkin para "<checkin>" e o período de checkout para "<checkout>" --reservation
    Então verifico se meio período está sendo apresentado corretamente --reservation
    E verifico se a quantidade de noites apresentada está conforme o que foi escolhido --reservation
    E efetuo uma reserva --reservation
    E verifico que estou na tela de pagamento de boarding --payment-boarding

    Exemplos:
    | checkin | checkout |
    | manhã   | noite    |

  Cenario: Validar novo pet adicionado
    Dado estou na tela de reserva com um anfitrião de testes e serviço de boarding --reservation
      | address | email  | password |
      | random  | client | default  |
    E altero a data de hospedagem --reservation
    Quando adiciono um novo pet --reservation
    Então o novo pet é apresentado com sucesso --reservation
    E desabilito um pet existente --reservation
    E o valor total é alterado com sucesso --reservation
    E habilito um pet existente --reservation
    E o valor total é alterado com sucesso --reservation