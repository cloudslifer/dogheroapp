#language: pt
#encoding: utf-8

@FullSmoke
@HostProfile
Funcionalidade: Host profile

  - Narrativa:
  Acessar o perfil do anfitrião a fim de verificar todas as suas informações para decidir se efetuo uma reserva ou não

  Cenário: Validar o perfil do anfitrião
    Dado estou na tela de busca com uma localização informada --search
      | address |
      | random  |
    Quando clico no perfil de um anfitrião aleatório --search
    Entao valido que estou no perfil do anfitrião --host-profile
    E que os dados apresentados na busca conferem com os de seu perfil --host-profile
    E clico na imagem da capa do anfitrião --host-profile
    E valido a galeria de fotos é apresentada com sucesso --host-profile

