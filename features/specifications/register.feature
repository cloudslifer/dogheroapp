#language: pt
#encoding: utf-8

@FullSmoke
@Register
Funcionalidade: Register

  - Narrativa:
  Eu como cliente preciso me cadastrar para usufruir de todas as funcionalidades do sistema

  Cenário: Validar cadastro
    Dado estou na tela inicial com uma localização informada --home
      | address |
      | random  |
    E vou para a tela de cadastro --login
    Quando efetuo meu cadastro com email "random" e senha "random" --register
    Entao o meu acesso é efetuado com sucesso --menu

  Cenário: Cadastro inválido
    Dado estou na tela inicial com uma localização informada --home
      | address |
      | random  |
    E vou para a tela de cadastro --login
    Quando efetuo meu cadastro com email "luis.vieira@doghero.com.br" e senha "random" --register
    Entao verifico que um alerta de erro foi apresentado ao tentar efetuar o cadastro --register