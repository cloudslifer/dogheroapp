#language: pt
#encoding: utf-8

@FullSmoke
@MyHeroes
Funcionalidade: My Heroes

  - Narrativa:
  Possibilitar que o cliente favorite seus heróis a fim de ter mais facilidade na recorrência dos serviços


  @ignoreIos
  Cenário: Verificar que herói é adicionado e removido com sucesso - search
    Dado favorito um anfitrião na tela de busca com uma localização informada e logado --search
      | address | email  | password |
      | random  | client | default  |
    Quando vou para a tela de meus heróis --search
    E estou na tela de meus heróis --my-heroes
    Então verifico que o herói adicionado na tela de 'search' foi adicionado com sucesso --my-heroes
    E removo o herói dos meus favoritos
    E o herói é removido com sucesso

  Cenário: Verificar que herói é adicionado e removido com sucesso - host-profile
    Dado favorito um anfitrião em seu perfil com uma localização informada e logado --host-profile
      | address | email  | password |
      | random  | client | default  |
    Quando vou para a tela de meus heróis --search
    E estou na tela de meus heróis --my-heroes
    Então verifico que o herói adicionado na tela de 'host_profile' foi adicionado com sucesso --my-heroes
    E removo o herói dos meus favoritos
    E o herói é removido com sucesso