module SpecPages
  class MyPets < SitePrism::Page

    def is_my_pets
      is_visible_el 'my_pets.is_my_pets'
    end


    def has_pets
      if is_visible_el 'my_pets.pet_card'
        @@pets_qty = find_all_els('my_pets.pet_card').size
        true
      else
        @@pets_qty = 0
        false
      end
    end

    def add_pet
      has_pets
      tap_el 'my_pets.add_pet'
    end

    def check_added_pet
      size = find_all_els('my_pets.pet_card').size

      size > @@pets_qty
    end

    def delete_pet
      has_pets

      click_random_el 'my_pets.pet_card'
      tap_el 'my_pets.delete_pet'
      tap_el 'my_pets.confirm_delete'
    end

    def check_deleted_pet
      size = find_all_els('my_pets.pet_card').size

      size < @@pets_qty
    end

    def fill_dog(name, sex, breed, breed_list, age, size, picture)
      is_visible_el 'my_pets.fill_dog_pet_name'
      send_keys_el 'my_pets.fill_dog_pet_name', random_data(name, 'dog_name')
      continue_dog_steps
      choose_dog_sex sex
      continue_dog_steps
      choose_breed breed, breed_list
      continue_dog_steps
      choose_age age
      continue_dog_steps
      choose_size size
      continue_dog_steps
      choose_picture picture
      continue_dog_steps

    end

    def success_pet_registration
      is_visible_el 'my_pets.success_pet_registration'
    end

    private
    def continue_dog_steps
      if device_controllers_keyboard.is_keyboard_shown
        device_controllers_keyboard.close_keyboard
      end

      if is_visible_el 'my_pets.fill_dog_continue_button'
        click_el 'my_pets.fill_dog_continue_button'
      end
    end

    def choose_dog_sex sex
      gender = ['my_pets.fill_dog_gender_male', 'my_pets.fill_dog_gender_female']

      case sex
      when 'male'
        tap_el gender[0]
      when 'female'
        tap_el gender[1]
      when 'random'
        tap_el gender.sample
      end

    end


    def choose_breed breed, breed_list

      if breed == 'random'
        breed = breed_list.sample
      end

      send_keys_el 'my_pets.choose_breed', breed
      device_controllers_keyboard.close_keyboard
    end

    def choose_age age
      if age == 'random'
        age = rand(5)
      end
      for i in 0..age
        click_el 'my_pets.age_up'
      end

    end


    def choose_size size

      if size == 'random'
        all_size = ['-5kg', '5kg - 10kg', '10kg - 20kg', '20kg - 40kg', '40kg+']
        size = all_size.sample
      end

      size = '20kg - 40kg'


      case size
      when '-5kg'
        el = 'my_pets.dog_size_less_5kg'
        clicks = 0
        text = '5kg'
      when '5kg - 10kg'
        el = 'my_pets.dog_size_5kg_10kg'
        clicks = 1
        text = '5kg a 10kg'
      when '10kg - 20kg'
        el = 'my_pets.dog_size_10kg_20kg'
        clicks = 2
        text = '10kg a 20kg'
      when '20kg - 40kg'
        el = 'my_pets.dog_size_20kg_40kg'
        clicks = 3
        text = '20kg a 40kg'
      when '40kg+'
        el = 'my_pets.dog_size_more_than_40kg'
        clicks = 4
        text = 'mais que 40kg'
      end

      clicks.times do
        is_visible_el 'my_pets.size_up'
        els = find_all_els 'my_pets.size_up'

        if els.size == 1
          els[0].click
        else
          els[1].click
        end

      end

      if IOS
       is_visible_el el
      else
        txt = get_text_el 'my_pets.dog_size'
        bool = text.downcase.include? txt
        bool
      end

    end


    def choose_picture picture

      ## todo
      # corrigir erro no browserstack
      picture = 'no'

      if picture == 'random'
        picture = ['yes', 'no'].sample
      end

      if picture == 'yes'
        click_el 'my_pets.open_picture_gallery'
        device_controllers_camera.upload_picture_from_gallery
      end

    end


    def get_all_breeds breeds
      breeds
    end

  end
end

