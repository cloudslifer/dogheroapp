module SpecPages
  class Payment < SitePrism::Page

    def is_payment
      is_visible_el 'payment.is_payment'
    end

    def get_total_price
      find_el 'payment.check_total_price'
      price = get_text_el 'payment.check_total_price'
      price.sub("R$",  "").sub("$", "")
      @@price = price.to_i
      @@price
    end

    def select_address
      swiper :down_android
      click_el 'payment.add_address'
    end

    def choose_payment_credit_card
      swiper :down_android
      swiper :down_android
      click_el 'payment.choose_credit_card'
    end

    def choose_payment_boleto
      swiper :down_android
      click_el 'payment.choose_boleto'
    end

    def select_credit_card
      swiper :down
      swiper :down

      click_el 'payment.credit_card_select_another_credit_card'
    end

    def choose_installments
      swiper :down_android
      click_el 'payment.credit_card_installments'

      if IOS
        $appium_driver.select_picker_wheel(element: find_el('payment.credit_card_installments_picker_wheel'), order: 'next', offset: nil)
      else
        click_random_el 'payment.credit_card_installments_options'
        send_keys_el 'payment.credit_card_confirm_cvv', get_credit_card_cvv
      end

    end

    def change_personal_data name, cpf, phone
      swiper :down_android
      clear_el 'payment.personal_data_name'
      send_keys_el 'payment.personal_data_name', random_data(name, 'name')

      clear_el 'payment.personal_data_cpf'
      send_keys_el 'payment.personal_data_cpf', random_data(cpf, 'cpf')

      clear_el 'payment.personal_data_cellphone'
      send_keys_el 'payment.personal_data_cellphone', random_data(phone, 'phone_number')
    end

    def finish_reservation
      click_el 'payment.finish_reservation'
    end

    def success_msg
      unless is_visible_el 'payment.finish_success'
        sleep 3
        is_visible_el 'payment.finish_success'
      end
      true
    end

    private

    def installments_options
      ## todo
      # picker wheel random
    end

  end
end