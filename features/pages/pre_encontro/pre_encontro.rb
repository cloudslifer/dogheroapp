module SpecPages
  class PreEncontro < SitePrism::Page

    def close_advise
      if is_visible_el 'pre_encontro.close_advise'
        click_el 'pre_encontro.close_advise'
      end
    end


    def is_pre_encontro_page
      is_visible_el 'pre_encontro.is_pre_encontro_page'
    end


  end
end