module SpecPages
  class Menu < SitePrism::Page

    def update_terms
      if ANDROID
        sleep 3
        2.times do
          if is_visible_el 'login.agree_updated_terms'
            tap_el 'login.agree_updated_terms'
            break
          end
        end
      end
    end

    def go_to_login
      click_el 'menu.access_login'
    end

    def go_to_my_pets
      click_el 'menu.access_my_pets'
    end

    def wanna_be_host
      click_el 'menu.be_a_host'
    end

    def is_logged
      if IOS
        swiper :down
        swiper :down
        is_visible_el 'menu.is_logged'
      else
        click_el 'menu.settings'
        is_visible_el 'menu.settings_is_logged'
      end
    end

    def do_logout
      if IOS
        click_el 'menu.logout'
      else
        click_el 'menu.settings_logout'
      end
    end
  end
end