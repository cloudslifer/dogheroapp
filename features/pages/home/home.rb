module SpecPages
  class Home < SitePrism::Page

    @text_file

    def initialize
      @text_file = get_text_file
    end

    def is_home
      bool = false
      2.times do
        bool = is_visible_el 'home.check_home_page'
      end

      unless bool
        menu.update_terms
      end

      bool = is_visible_el 'home.check_home_page'
      bool
    end

    def check_home_page
      texts = %w{what_your_dog_needs what_your_dog_needs_2 get_updated promotions}

      assert_texts_from_file(@text_file, texts)
    end

    def check_bottom_menu_not_logged
      find_el 'home.bottom_menu_choose_menu'
      find_el 'home.bottom_menu_choose_home'

      res = []
      res << find_exists_fast('home.bottom_menu_choose_inbox')
      res << find_exists_fast('home.bottom_menu_choose_my_heroes')

      res.include? true ?  false : true
    end

    def check_bottom_menu_logged
      find_el 'home.bottom_menu_choose_menu'
      find_el 'home.bottom_menu_choose_home'
      find_el 'home.bottom_menu_choose_inbox'
      find_el 'home.bottom_menu_choose_my_heroes'
      true
    end

    def go_to_boarding
      click_el 'home.go_to_boarding'
    end

    def go_to_dog_walking
      click_el 'home.go_to_dog_walking'
    end

    def back_to_home
      click_el 'home.bottom_menu_choose_home'
    end

    def open_menu
      5.times do
        if is_visible_el 'home.bottom_menu_choose_menu'
          sleep 2
          break
        else
          sleep 1
        end
      end

      click_el 'home.bottom_menu_choose_menu'
    end

    def go_to_my_heroes
      click_el 'home.bottom_menu_choose_my_heroes'
    end

    def check_address_is_filled
      get_text_el('home.current_address').empty? ?  false: true
    end

    def change_address

      3.times do
        if is_visible_el('home.current_address')
          sleep 2
          break
        end
      end

      tap_el 'home.current_address'

    end
    def check_main_features
      find_el 'home.go_to_boarding'

      if COUNTRY == 'br'
        find_el 'home.go_to_dog_walking'
      end
      true
    end
  end


  private
  def get_text_file
    txt = YAML.load_file('features/pages/home/home_texts.yml')
    txt
  end

end