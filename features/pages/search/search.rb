module SpecPages
  class Search < SitePrism::Page

    @text_file

    def initialize
      @text_file = get_text_file
    end

    def is_search
      allow_location
      #deny_notification
      close_tooltip
      is_visible_el 'search.is_search'
    end

    def close_tooltip
      if is_visible_el 'search.container_tooltip'
        tap_el 'search.container_tooltip'
      end
    end

    def go_to_random_host_profile
      els = get_random_host_profile

      if ANDROID
        click_solved_el els[@@rand_el]
      else
        click_solved_el @@click_host
      end
    end

    def get_random_host_profile
      find_el 'search.valid_result'
      els = find_all_els 'search.valid_result'

      els.size > 1? size= 1 : size = els.size

      @@rand_el = rand(0..size)

      set_host_info @@rand_el

      els
    end

    def set_host_info host_index
      $search_result_host_name = find_all_els 'search.result_first_name_label'
      $search_result_host_boarding_price = find_all_els 'search.result_price_label'

      @@click_host = $search_result_host_name[host_index]

      $search_result_host_name = $search_result_host_name[host_index].text.strip
      $search_result_host_boarding_price = $search_result_host_boarding_price[host_index].text.sub("R$", "").sub("$", "").strip

      @@favorite_button = host_index
    end


    def change_address
      click_el 'search.current_address'
    end

    def change_date
      click_el 'search.change_date'

      if IOS
        choose_checkin_date_ios 'search.date_choose_day'
        choose_checkout_date_ios 'search.date_choose_day', 'search.date_ok'
      else
        choose_checkin_date 'search.date_choose_day', 'search.date_ok'
        choose_checkout_date 'search.date_choose_day', 'search.date_ok'
      end
    end


    def change_date_filter
      click_el 'search.filters_change_date'

      if IOS
        choose_filter_checkin_and_checkout_date_ios 'search.filters_date_choose_day', 'search.filters_date_ok'
      else
        choose_checkin_date 'search.filters_date_choose_day', 'search.filters_date_ok'
        choose_checkout_date 'search.filters_date_choose_day', 'search.filters_date_ok'
      end
    end

    def is_valid_result
      if IOS
        find_el 'search.valid_result'
        true
      else
        is_visible_el 'search.valid_result'
      end
    end

    def check_result
      result = {}

      find_el 'search.result_first_name_label'
      result['name'] = has_text_el 'search.result_first_name_label'
      find_el 'search.result_title_label'
      result['title'] = has_text_el 'search.result_title_label'
      find_el 'search.result_address_label'
      result['address'] = has_text_el 'search.result_address_label'
      find_el 'search.result_price_label'
      result['price'] = has_text_el 'search.result_price_label'

      ok = true
      failed_assertion = {}
      result.each do |key, value|
        unless value
          ok = false
          failed_assertion[key] = value
        end
      end

      if ok
        true
      else
        log_error "Erro na checagem dos textos: #{failed_assertion}"
        false
      end
    end

    def open_host_profile
      if random_boolean
        times = [1,2,3].sample
        swipe_search_page times
      end

      click_random_el 'search.valid_result'
    end

    def swipe_pagination
      times = [2,3,4].sample
      swipe_search_page times
    end

    def favorite_hero
      get_random_host_profile
      els = find_all_els 'search.result_favorite_hero'
      el = els[@@favorite_button]
      if IOS
        ##TODO
        # corrigir
        tap_solved_el find_all_els('search.result_favorite_hero')[@@favorite_button]
      else
        el.click
      end
    end

    def exit_profile
      if IOS
        click_el 'host_profile.close_profile'
      else
        press_back
      end
    end



    def open_filters
      click_el 'search.open_filters'
    end

    def choose_filters(pet_kind='random', pet_size='random', pet_sex='random', pet_spayed='random', env_type='random', ext_area='random', has_dogs='random', others='random')
      click_filter 'search.filters_pet_kind', pet_kind
      click_filter 'search.filters_pet_size', pet_size
      swiper :down
      click_filter 'search.filters_pet_sex', pet_sex
      click_filter 'search.filters_pet_spayed', pet_spayed
      swiper :down
      click_filter 'search.filters_environment_type', env_type
      swiper :down
      click_filter 'search.filters_others', others
      apply_filter
    end

    def close_filters
      click_el 'search.filters_back'
    end

    def clear_filters
      click_el 'search.filters_clear'
    end

    def apply_filter
      click_el 'search.apply_filter'
    end

    def open_map
      click_el 'search.open_maps'
    end

    def check_map
      is_visible_el 'search.maps_close'
      find_el 'search.maps_close'
      find_el 'search.maps_points'
      find_el 'search.maps_hosts_recommendations'
      true
    end

    def open_host_from_map
      click_el 'search.maps_hosts_recommendations'
    end

    private

    def deny_notification
      if IOS
        if is_visible_el 'search.get_notifications'
          click_el 'search.get_notifications_not'
        end

      end
    end

    def swipe_search_page time
      time.times do
        swiper :down
        is_valid_result
      end
    end

    def choose_filter_checkin_and_checkout_date_ios day, apply
      elements = find_all_els day
      checkin = rand(1..elements.size)
      checkin_day = elements[checkin]
      tracing "Data de checkin escolhida foi dia: #{checkin_day.text}"
      checkin_day.click

      checkout = rand(checkin..elements.size)
      checkout_day = elements[checkout]

      tracing "Data de checkout escolhida foi dia: #{checkout_day.text}"
      checkout_day.click

      tap_el apply
    end

    def choose_checkin_date_ios day
      availables = []

      elements = find_all_els(day)
      elements.each do | element|
        availables << element
      end


      loop do
        @@random_day_checkin = rand(0..elements.size)
        @@checkin = elements[@@random_day_checkin]
        break unless @@checkin.text.empty?
      end

      tracing "Data de checkin escolhida foi dia: #{@@checkin.text}"
      @@checkin.click
    end

    def choose_checkout_date_ios day, ok_button
      availables = []

      elements = find_all_els(day)
      elements.each do | element|
        availables << element
      end

      loop do
        random_day_checkout = rand(@@random_day_checkin..elements.size)
        @@checkout = elements[random_day_checkout]
        break unless @@checkout.text.empty?
      end

      tracing "Data de checkout escolhida foi dia: #{@@checkout.text}"
      @@checkout.click

      click_el ok_button
    end


    def choose_checkin_date day, ok_button
      availables = []

      elements = find_all_els(day)
      elements.each do | element|
        if element.enabled?
          availables << element
        end
      end

      el = availables.sample
      @@checkin = el.text.to_i
      click_solved_el el
      tracing "Data de checkin escolhida foi dia: #{@@checkin}"
      click_el ok_button

    end


    def choose_checkout_date day, ok_button
      availables = []

      elements = find_all_els(day)
      elements.each do | element|
        if element.enabled?
          if element.text.to_i >= @@checkin
          availables << element
          end
        end
      end

      el = availables.sample
      tracing "Data de checkout escolhida foi dia: #{el.text}"
      click_solved_el el
      click_el ok_button

    end

    def click_filter el, filter
      if filter=='random'
        els = find_all_els el
        els.each do |el|
          if random_boolean
            click_solved_el el
          end
        end

      end
    end


    def get_text_file
      txt = YAML.load_file('features/pages/search/search_texts.yml')
      txt
    end

    def allow_location
      if ANDROID
        if is_visible_el 'search.allow_location_dh'
          click_el 'search.allow_location_dh'
          click_el 'search.allow_location_android'
          click_el 'search.allow_location_android'
        end
      end
    end
  end
end













