module SpecPages
  class MyCreditCards < SitePrism::Page

    def has_credit_cards
      if is_visible_el 'my_credit_cards.has_credit_cards'
        els = find_all_els 'my_credit_cards.has_credit_cards'
        @@credit_cards_qty = els.size
        true
      else
        @@credit_cards_qty = 0
        false
      end
    end

    def add_new_credit_card
      click_el 'my_credit_cards.add_new_credit_card'
    end

    def fill_new_credit_card holder
      send_keys_el 'my_credit_cards.new_credit_card_number',get_credit_card_from_yml(holder)
      send_keys_el 'my_credit_cards.new_credit_card_name', random_data('random', 'name')
      set_date
      send_keys_el 'my_credit_cards.new_credit_card_cvv', get_credit_card_cvv
      send_keys_el 'my_credit_cards.new_credit_card_cpf', random_data('random', 'cpf')
    end

    def set_date
      if IOS
        send_keys_el 'my_credit_cards.new_credit_card_date', '022020'
      else
        click_el 'my_credit_cards.new_credit_card_month'
        months = find_all_els 'my_credit_cards.new_credit_card_month_options'
        months.each do | month |
         if month.text == '12'
           month.click
         end
        end

        click_el 'my_credit_cards.new_credit_card_year'
        years = find_all_els 'my_credit_cards.new_credit_card_year_options'
        years.each do | year |
          if year.text == '2022'
            year.click
          end
        end
      end
    end

    def save_new_credit_card
      click_el 'my_credit_cards.new_credit_card_save'
    end

    def check_credit_card_added
      els = find_all_els 'my_credit_cards.has_credit_cards'
      cards = els.size

      cards > @@credit_cards_qty
    end

    def select_random_credit_card
      if has_credit_cards
        click_random_el 'my_credit_cards.has_credit_cards'
        confirm_credit_card
      else
        add_new_credit_card
        fill_new_credit_card 'random'
        save_new_credit_card
        check_credit_card_added
        click_random_el 'my_credit_cards.has_credit_cards'
      end
    end

    def confirm_credit_card
      click_el 'my_credit_cards.new_credit_card_select'
    end

  end
end