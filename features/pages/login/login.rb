module SpecPages
  class Login < SitePrism::Page

    def do_login(email, password)
      get_login email, password
      send_keys_el 'login.email', @@email
      send_keys_el 'login.password', @@password

      if device_controllers_keyboard.is_keyboard_shown
        device_controllers_keyboard.close_keyboard
      end
      click_el 'login.do_login'
    end


    def is_invalid_login
      ## todo
      # arrumar o element toast
      is_visible_el 'login.invalid_login_error'
    end

    def go_to_register
      device_controllers_keyboard.close_keyboard
      click_el 'login.go_to_register'
    end

    private

    def get_login(email, password)
      case email.downcase
      when 'client'
        @@email = CLIENT_EMAIL
        @@password = CLIENT_PASSWORD
      when 'host'
        @@email = HOST_EMAIL
        @@password = HOST_PASSWORD
      else
        @@email = email
        @@password = password
      end

    end

  end
end