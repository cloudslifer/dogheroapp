module SpecPages
  class Conversation < SitePrism::Page

    def is_conversation
      #close_advice
      is_visible_el 'conversation.is_conversation'
    end

    def close_advice
      if is_visible_el 'conversation.ok_reminder'
        sleep 2
        tap_el 'conversation.ok_reminder'
      end

    end

    def check_pet_added
      els = find_all_els('conversation.pet_added')
      if IOS
        els.size-1 > @@pets_qty
      else
        els.size > @@pets_qty
      end
    end


    def checkin_period period
      swiper :down
      click_el 'conversation.set_checkin'

      set_period 'all_available_days', period, 'conversation.set_checkin_options'
    end

    def checkout_period period
      click_el 'conversation.set_checkout'

      set_period 'checkout', period, 'conversation.set_checkout_options'
    end

    def check_nights_qty
      nights = get_nights_qty
      if @@nights == nights
        true
      else
        tracing "Erro! Quantidade de noites escolhidas foi: #{@@nights}. Quantidade de noites apresentada é de: #{nights}"
        false
      end
    end

    def check_price
      nights = get_nights_qty
      host_price = $host_profile_boarding_price.to_i

      pets_qty = pets_enabled

      @@price = get_price
      if @@price == 0
        switch_pets 'enable'
        @@price = get_price
      end

      tracing "Preço apresentado: #{@@price}"
      tracing "Quantidade de pets: #{pets_qty}"

      if @@same_day || !@@half_period
        total_price = nights * host_price
        total_price = total_price * pets_qty

        tracing "Preço calculado: #{total_price}"

        @@price == total_price

      else
        tracing "Verificando meio período"
        half_value = host_price / 2
        half_value = half_value * pets_qty
        tracing "Valor cobrado por noite é de: #{host_price}. Meio período deve ser: #{half_value}"
        total_price = nights * host_price
        total_price = total_price * pets_qty + half_value

        tracing "Preço calculado: #{total_price}"

        @@price == total_price

      end
    end

    def pets_enabled
      swiper :down
      unless is_visible_el 'conversation.pet_enable'
        swiper :down
      end

      pets = find_all_els 'conversation.pet_enable'

      pets_qty = 0

      if ANDROID
        pets.each do | pet |
          txt = pet.text.downcase
          if txt == 'lig' || txt == 'lig.' || txt == 'ligado' || txt == 'ativado'
            pets_qty+= 1
          elsif random_bool
            click_solved_el pet
            pets_qty+= 1
          end
        end

      else
        pets.each do | pet |
          if pet.attribute('value') == '1'
            pets_qty+= 1
          elsif random_bool
            click_solved_el pet
            pets_qty+= 1
          end
        end
      end

      if pets_qty == 0
        click_solved_el pets[0]
        pets_qty + 1
      end

      pets_qty
    end

    def set_period period, type, element
      els = find_all_els element

      type.downcase!

      txts = ["Manhã (6h às 11h)", "Tarde (12h às 18h)", "Noite (19h às 23h)"]
      if IOS
        txts = ['Pela manhã', 'Pela tarde', 'Pela noite']
      end

      case type
      when 'random'
        el = els.sample
        text = txts.sample

        ANDROID ? choosed_period = el.text : choosed_period = text

        if period == 'all_available_days'
          choosed_period.include?('Manhã') ? @@morning_period = true : @@morning_period = false
        elsif period == 'checkout'
          choosed_period.include?('Noite') ? @@night_period = true : @@night_period = false
        end

      when 'manhã'
        el = els[0]
        text = txts[0]
        if period == 'all_available_days'
          text.downcase.include?('manhã') ? @@morning_period = true : @@morning_period = false
        end
      when 'tarde'
        el = els[1]
        text = txts[1]
      when 'noite'
        el = els[2]
        text = txts[2]
        if period == 'checkout'
          text.downcase.include?('noite') ? @@night_period = true : @@night_period = false
        end
      end

      if IOS
        send_keys_el element, text
        click_el 'conversation.set_checkin_options_picker_wheel_ok'
      else
        click_solved_el el
      end
    end

    def check_half_period
      get_nights_qty

      if @@same_day
        if @@half_period
          tracing "Meio período não deveria ser apresentado para reserva de apenas um dia"
          false
        else
          tracing "OK! Meio período não apresentado conforme o esperado"
          true
        end
      elsif @@morning_period && @@night_period

        @@half_period
      else
        if @@half_period
          tracing_error "Meio período não deveria ser apresentado, apenas caso o checkin seja de manhã e checkout a noite"
          false
        else
          tracing "OK! Meio período não apresentado conforme o esperado"
          true
        end
      end
    end

    def set_days
      tap_el 'conversation.set_days'

      days = find_all_els 'conversation.calendar_choose_available_day'

      all_available_days = []
      if ANDROID
        days.each do | day|
          if day.enabled?
            all_available_days << day
          end
        end
      else
        all_available_days = days
      end

      rand_checkin = rand(0..all_available_days.size-1)
      day_checkin = all_available_days[rand_checkin]
      @@checkin = day_checkin.text.to_i

      click_solved_el day_checkin

      if IOS
        days = find_all_els 'conversation.calendar_choose_available_day'
        all_available_days = days
      end

      rand_checkout = rand(rand_checkin..all_available_days.size-1)
      day_checkout = all_available_days[rand_checkout]
      @@checkout = day_checkout.text.to_i

      if @@checkin == @@checkout
        @@nights = 1
      else
        click_solved_el day_checkout
        @@nights = rand_checkout - rand_checkin
      end

      tracing "Checkin: #{@@checkin}. Checkout: #{@@checkout}"
      tracing "Quantidade de noites de hospedagem: #{@@nights}"

      @@checkin == @@checkout ? @@same_day = true : @@same_day = false

      click_el 'conversation.apply_date'

    end

    def pre_meeting option
      option.downcase!

      unless is_visible_el 'conversation.pre_meeting_no'
        swipe :down
      end

      if ANDROID
        case option
        when 'random'
          click_random_el 'conversation.pre_meeting_no'
        when 'não', 'no'
          find_all_els('conversation.pre_meeting_no')[1].click
        when 'sim', 'yes'
          find_all_els('conversation.pre_meeting_no')[0].click
        end

      else
        pre_meeting_options = ['conversation.pre_meeting_no', 'conversation.pre_meeting_yes']

        case option
        when 'random'
          el = pre_meeting_options.sample
        when 'não' , 'no'
          el = pre_meeting_options[0]
        when 'sim' , 'yes'
          el = pre_meeting_options[1]
        end

        click_el el

      end
    end

    def add_new_pet
      sleep 1
      swiper :down
      swiper :down
      swiper :down
      swiper :down
      if ANDROID
        if is_visible_el 'conversation.pet_added'
          pets = find_all_els 'conversation.pet_added'
          @@pets_qty = pets.size
        end
      elsif IOS
        pets = find_all_els 'conversation.pet_added'
        @@pets_qty = pets.size-1
      else
        @@pets_qty = 0
      end

      click_el 'conversation.add_new_pet'
    end

    def has_pets
      is_visible_el 'conversation.pet_added'
    end

    def switch_pets action
      swiper :down
      @@price = get_price

      if ANDROID
        action=='enable' ? action='des' : action='lig'
      else
        action=='enable' ? action='0' : action='1'
      end

      switcher action

      @@new_price = get_price
    end

    def switcher action
      swiper :down

      els = find_all_els 'conversation.pet_enable'

      if IOS
        els.each do | el |
          if el.attribute('value') == action
            el.click
            break
          end

        end
      else
        lig = ['ativado', 'ligado', 'lig', 'lig.']
        des = ['desligado', 'desativado', 'des']

        action=='lig' ? list = lig : list = des

        els.each do | el |

          txt = el.text.downcase

          puts "txt do elemento é: #{txt}"

          list.each do | act |

            if txt == act
              el.click
              break
            end
          end

        end

      end
    end

    def compare_prices action
      tracing "Valor antes da alteração de pets #{@@price}"
      tracing "Valor após alteração de pets #{@@new_price}"

      if action == 'enable'
        @@new_price > @@price
      else
        @@new_price < @@price
      end
    end

    def write_msg msg
      swiper :down
      send_keys_el 'conversation.write_msg', random_data(msg, 'paragraph')
      device_controllers_keyboard.ok_keyboard
    end

    def send_msg
      click_el 'conversation.send_msg'
    end

    def is_msg_sent
      if is_visible_el 'conversation.msg_sent'
        true
      else
        sleep 5
        is_visible_el 'conversation.msg_sent'
      end

    end

    def is_boarding_active
      if IOS
        unless is_visible_el 'conversation.is_boarding_active'
          click_el 'conversation.choose_service'
          send_keys_el 'conversation.choose_service_picker_wheel', 'Hospedagem'

          is_visible_el 'conversation.is_boarding_active'
        end
        true
      else
        txt = get_text_el 'conversation.is_boarding_active'
        unless txt.downcase.include? 'hospedagem'
          click_el 'conversation.choose_service'
          click_el 'conversation.choose_boarding_service'

          get_text_el('conversation.is_boarding_active').downcase.include? 'hospedagem'
        end

        true
      end
    end

    def get_price
      price = get_text_el 'conversation.price'
      price = price.sub("Total:", "").sub("R$", "").sub("$", "").strip
      price = price.sub(".", "")
      price = price.split(",")
      price[0].to_i
    end

    def get_nights_qty
      nights = get_text_el 'conversation.price_description'
      nights.include?("meia") ? @@half_period = true : @@half_period = false
      nights = nights.sub("noites", "").sub("noite", "").sub("+ meia diária", "").strip
      nights.to_i
    end

    def check_error_alerts
      is_visible_el 'conversation.alert_error'
    end

  end
end