module SpecPages
  class MyHeroes < SitePrism::Page

    def is_my_heroes
      is_visible_el 'my_heroes.is_my_heroes'
    end

    def check_has_favorite_heroes
      is_visible_el 'my_heroes.has_favorite_heroes'
    end

    def go_to_qa_hero
      el = get_hero_by_name('Qa')
      click_solved_el el
    end

    def check_hero_was_added page
      screen_name = ''
      case page
      when 'search'
        screen_name = 'search'
        page_name = $search_result_host_name
      when 'host_profile'
        screen_name = 'host_profile'
        page_name = $host_profile_name

      end

      @@page_name = page_name

      el = get_hero_by_name @@page_name

      unless el.nil?
        true
      else
        tracing_error "O anfitrião: #{@@page_name} adicionado pela tela #{screen_name} não foi encontrado na página de favoritos"
      end
    end

    def remove_hero
      el = get_hero_by_name @@page_name
      favorite_button = find_child_el(el, 'my_heroes.host_favorite_button')
      click_solved_el favorite_button
    end

    def check_hero_was_removed
      el = get_hero_by_name @@page_name

      el.nil?
    end


    def get_hero_by_name hero_name
      element = nil
      els = find_all_els 'my_heroes.has_favorite_heroes'

      els.each do |el|
        name = find_child_el(el,'my_heroes.host_name')
        #name.size > 0 ? name = name[1] : name[0]
        txt = get_text_solved_el(name, 'nome do heroi')

        if txt == hero_name
          element = el
          break
        end
      end
      element
    end

    def check_hero_info
      els = find_all_els 'my_heroes.has_favorite_heroes'

      els.each do | el|
        find_child_el(el, 'my_heroes.host_name')
        find_child_el(el, 'my_heroes.host_location')
        find_child_el(el, 'my_heroes.host_favorite_button')
        price = find_child_el(el, 'my_heroes.host_price')
        if price == "--"
          log_error "Preço não está sendo apresentado"
        end
        find_child_el(el,'my_heroes.go_to_conversation')
        find_child_el(el, 'my_heroes.go_to_reservation')
      end

      true
    end

  end
end