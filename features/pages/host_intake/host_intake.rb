module SpecPages
  class HostIntake < SitePrism::Page

  def wanna_be_host
    click_el 'host_intake.wanna_be_host'
    sleep 5
  end

  def choose_basic_info
    click_el 'host_intake.choose_basic_info'
  end


  def fill_basic_info

    send_keys_el 'host_intake.basic_info_birthday', '19/09/1990'
    click_el 'host_intake.basic_info_male_sex'


    send_keys_el 'host_intake.basic_info_phone', '1133993399'

    send_keys_el 'host_intake.basic_info_address_name', 'Casa'

    send_keys_el 'host_intake.basic_info_address', 'Avenida Raimundo Pereira de Magalhaes'

    send_keys_el 'host_intake.basic_info_address_number', '1720'

    send_keys_el 'host_intake.basic_info_additional_info', 'Piqueri'

    send_keys_el 'host_intake.basic_info_neighbourhood', 'Jd Iris'

    send_keys_el 'host_intake.basic_info_city', 'São Paulo'

    send_keys_el 'host_intake.basic_info_state', 'São Paulo'

    send_keys_el 'host_intake.basic_info_cep', '05145-100'

    send_keys_el 'host_intake.basic_info_country', 'Brasil'

    send_keys_el 'host_intake.basic_info_emergency_contact', 'Mae'

    send_keys_el 'host_intake.basic_info_emergency_contact_number', '11909090900'

    send_keys_el 'host_intake.basic_info_occupation', 'TI'

    click_el 'host_intake.next_step'


  end



  def choose_profile_picture
    click_el 'host_intake.choose_profile_picture'
    upload_picture
  end

  def upload_picture
    device_controllers_camera.choose_picture_gallery
    device_controllers_camera.upload_picture_from_gallery
  end



  end
end