module SpecPages
  class Location < SitePrism::Page

    def set_address location

      send_keys_el 'location.set_address', random_data(location, 'address')
      if IOS
        send_keys_el 'location.set_address', "\n"
      end

      choose_auto_complete_address
    end

    def use_current_location
      click_el 'location.use_my_location'
      allow_current_location
    end

    private

    def choose_auto_complete_address
      if ANDROID
        if is_visible_el 'location.one_address'
          click_el 'location.one_address'
        else
          click_el 'location.choose_first_address'
        end
      else
        click_el 'location.choose_first_address'
      end

    end

    def allow_current_location
        if IOS
          click_el 'location.location_modal_allow'
          if is_visible_el 'location.location_modal_system_alert'
            tap_position_el 'location.location_modal_system_allow', 0
          end
        end
    end







  end
end