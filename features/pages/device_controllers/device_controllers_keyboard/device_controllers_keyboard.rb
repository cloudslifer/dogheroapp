module SpecPages
  class DeviceControllersKeyboard < SitePrism::Page

    def is_keyboard_shown
      $appium_driver.is_keyboard_shown
    end

    def close_keyboard
      $appium_driver.hide_keyboard rescue StandardError
    end

    def ok_keyboard
      if IOS
        click_el 'device_controllers_keyboard.keyboard_done'
      end
    end


  end
end