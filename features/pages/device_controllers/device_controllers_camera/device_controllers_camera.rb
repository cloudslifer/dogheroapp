module SpecPages
  class DeviceControllersCamera < SitePrism::Page



    def upload_picture_from_gallery
      upload_picture_to_emulator
      android_media_permission
      open_camera_album
      choose_picture_camera
      confirm_selected_picture
    end

    def upload_picture_to_emulator
      pic = ["DogProfile1.jpg", "DogProfile2.jpeg", "DogProfile3.jpg"]
      final_pic = pic.sample
      contents = File.read("dog_pictures/#{final_pic}")
      if ANDROID
        $appium_driver.push_file("/mnt/sdcard/Pictures/#{final_pic}", contents)
      else
        $appium_driver.push_file(final_pic, contents);
      end
    end

    def android_media_permission
      unless REMOTE
        if ANDROID
          if is_visible_el 'device_controllers_camera.accept_media_permission'
            click_el 'device_controllers_camera.accept_media_permission'
            click_el 'device_controllers_camera.google_accept_media_permission'
            click_el 'device_controllers_camera.google_accept_media_permission'
            true
          end
        end
        false
      else
        true
      end
    end


    def choose_picture_gallery
      click_el 'device_controllers_camera.choose_picture_gallery'
    end

    def choose_open_camera
      if IOS
        click_el 'device_controllers_camera.choose_camera'
      end
    end

    def open_camera_album
      click_el 'device_controllers_camera.camera_album'
    end

    def choose_picture_camera
      if IOS
        click_position_el 'device_controllers_camera.choose_picture_camera', 1
      else
        click_el 'device_controllers_camera.choose_picture_camera'
      end
    end


    def confirm_selected_picture
      if IOS
        click_position_el 'device_controllers_camera.choose_selected_picture', 1
      else
        click_el 'device_controllers_camera.choose_selected_picture'
      end
    end


  end
end