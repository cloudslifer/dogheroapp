module SpecPages
  class MyAddresses < SitePrism::Page


    def has_addresses
      if is_visible_el 'my_addresses.has_address'
        @@addresses_qty = find_all_els('my_addresses.has_address').size
        true
      else
        @@addresses_qty = 0
        false
      end
    end

    def new_address
      click_el 'my_addresses.new_address'
    end


    def fill_address address
      click_random_el 'my_addresses.new_address_type'
      send_keys_el 'my_addresses.new_address_name', random_data('random', 'name')

      click_el 'my_addresses.new_address_open_location'
      location.set_address address

      send_keys_el 'my_addresses.new_address_number', '123'

=begin
      if get_text_el('my_addresses.new_address_cep').empty?
        send_keys_el 'my_addresses.new_address_cep', '05145-100'
      end

      if get_text_el('my_addresses.new_address_neighborhood').empty?
        send_keys_el 'my_addresses.new_address_neighborhood', 'Jd Doghero'
        send_keys_el 'my_addresses.new_address_city', 'São Paulo'
        send_keys_el 'my_addresses.new_address_country', 'Brasil'

      end
=end

      device_controllers_keyboard.ok_keyboard
    end

    def save_address
      click_el 'my_addresses.new_address_save'
    end

    def check_address_added
      qty = find_all_els('my_addresses.has_address').size

      qty > @@addresses_qty
    end

    def select_random_address
      if has_addresses
        click_random_el 'my_addresses.has_address'
      else
        new_address
        fill_address 'random'
        save_address
        check_address_added
      end
    end

    def confirm_address
      click_el 'my_addresses.confirm_address'
    end













  end
end