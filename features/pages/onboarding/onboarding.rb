module SpecPages
  class Onboarding < SitePrism::Page

    def continue_welcome
      is_visible_el 'onboarding.welcome_continue'
      click_el 'onboarding.welcome_continue'
    end


    def skip_onboarding
      click_el 'onboarding.skip_onboarding'
    end

    def have_dog
      click_el 'onboarding.have_dog'
    end

    def do_not_have_dog
      click_el 'onboarding.do_not_have_dog'
    end

  end
end