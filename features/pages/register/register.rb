module SpecPages
  class Register < SitePrism::Page

    @text_file

    def initialize
      @text_file = get_text_file
    end

    def register_page_validation
      texts = %W{title information whatsapp_notifications}

      assert_texts_from_file(@text_file, texts)
    end

    def is_register
      is_visible_el 'register.is_register'
    end

    def register_user email, password

      send_keys_el 'register.email', random_data(email, 'email')

      send_keys_el 'register.name', random_data('random', 'name')

      send_keys_el 'register.surname', random_data('random', 'last_name')

      send_keys_el 'register.phone', '11999998888'

      send_keys_el 'register.password', default_password(password)

      how_met

      if random_boolean
        disable_whats_app_notification
      end

      device_controllers_keyboard.close_keyboard

      finish_sign_up
    end

    def how_met
      click_el 'register.how_met'

      if IOS
        send_keys_el 'register.how_met_picker_wheel', how_met_options
      else
        click_random_el 'register.how_met_picker_wheel'
      end

    end


    def how_met_options
      options = ['Blog', 'DogHero', 'Facebook', 'Família', 'Amigos', 'Google', 'Instagram', 'Jornal', 'Petlove', 'Rádio', 'Site de notícias', 'TV', 'Twitter', 'YouTube']
      options.sample
    end

    def disable_whats_app_notification
      if is_enabled_notification
        click_el 'register.whatsapp_notifications_enabled'
        disable_not_whats_app_modal
      end

    end

    def disable_not_whats_app_modal
      click_el 'register.whatsapp_notifications_do_not_want_to_be_notified'
    end

    def is_disabled_notification
      is_visible_el 'register.whatsapp_notifications_disabled'
    end

    def is_enabled_notification
      if IOS
        is_visible_el 'register.whatsapp_notifications_enabled'
      else
        is_checked_el 'register.whatsapp_notifications_checkbox'
      end
    end

    def finish_sign_up

      click_el 'register.register'
    end

    def invalid_register
      find_el('register.invalid_register')
      true
    end

    private
    def get_text_file
      txt = YAML.load_file('features/pages/register/register_texts.yml')
      txt
    end

  end
end