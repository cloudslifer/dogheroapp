module SpecPages
  class HostProfile < SitePrism::Page

    @text_file

    def initialize
      @text_file = get_text_file
    end

    def is_host_profile
      if ANDROID
        sleep 4
        is_visible_el 'host_profile.is_host_profile'
        true
      else
        find_all_els('host_profile.is_host_profile').size > 0
      end


    end

    def has_boarding
      is_visible_el 'host_profile.has_boarding'
    end

    def check_data_from_search
      set_host_info

      unless $search_result_host_name == $host_profile_name
        tracing_error "Nome do anfitrião na search page é: #{$search_result_host_name} e no perfil: #{$host_profile_name}"
      end

      unless $search_result_host_boarding_price == $host_profile_boarding_price
        tracing_error "Preço de boarding do anfitrião na search page é: #{$search_result_host_boarding_price} e no perfil: #{$host_profile_boarding_price}"
      end

      true
    end

    def go_to_conversation
      set_host_info
      tap_el 'host_profile.go_to_conversation'
    end

    def go_to_reservation
      set_host_info
      tap_el 'host_profile.go_to_reservation'
    end

    def favorite_host
      set_host_info
      @@favorite_button.click
    end

    def open_gallery
      if ANDROID
        @@total_imgs = get_text_el 'host_profile.gallery_check_total_pictures'
        @@total_imgs = @@total_imgs.split("/")
        @@total_imgs = @@total_imgs[1].to_i

        sleep 2

        tap_el 'host_profile.open_gallery'
      else
        click_el 'host_profile.open_gallery'
      end
    end

    def check_gallery
      find_el 'host_profile.gallery_check_all_pictures'
      click_random_el 'host_profile.gallery_check_all_pictures'
      is_visible_el 'host_profile.gallery_check_opened_picture'
    end

    def change_picture
      testable = true

      if IOS
        pictures = get_text_el 'host_profile.gallery_check_total_pictures'
        pictures = pictures.to_i
      else
        pictures = @@total_imgs
      end

      tracing "Total de fotos do anfitrião: #{pictures.to_s}"

      loops = 3
      if pictures <= 3
        if pictures == 1
          testable = false
        else
          loops = pictures
        end
      end


      if testable
        loops.times do
          current_picture = get_current_picture
          swiper :left
          new_picture = get_current_picture

          if new_picture == pictures.to_i
            break
          end

          unless new_picture > current_picture
            tracing_error "Erro ao passar para a próxima image. Imagem inicial era: #{current_picture}, a nova imagem é #{new_picture}"
          end
        end

      else
        tracing "Apenas uma imagem encontrada. Passo não pode ser executado"
      end
      true

    end


  private
    def set_host_info
      if IOS

        $host_profile_name = find_all_els 'host_profile.host_name'

        $host_profile_boarding_price = find_all_els 'host_profile.host_boarding_price'

        @@favorite_button = find_all_els 'host_profile.host_favorite_button'

        size = $host_profile_name.size
        if size  <=2
          $host_profile_name = $host_profile_name[0].text
          $host_profile_boarding_price = $host_profile_boarding_price[0].text
          @@favorite_button = @@favorite_button[0]
        else
          $host_profile_name = $host_profile_name[1].text
          $host_profile_boarding_price = $host_profile_boarding_price[1].text
          @@favorite_button = @@favorite_button[1]
        end
      else
        @@favorite_button = find_el 'host_profile.host_favorite_button'
        get_text_el 'host_profile.host_name'
        $host_profile_boarding_price = get_text_el 'host_profile.host_boarding_price'
        $host_profile_name = get_text_el 'host_profile.host_name'
      end


        $host_profile_boarding_price = $host_profile_boarding_price.sub("R$", "").sub("$", "").strip
    end

    def get_current_picture
      if IOS
        picture = get_text_el 'host_profile.gallery_check_current_picture'
        picture = threat_current_picture picture
        picture
      else
        picture = find_el 'host_profile.gallery_check_opened_picture'
        picture.ref.to_i
      end
    end
    def threat_current_picture current_picture
      picture = current_picture.split("de")
      picture[0].to_i
    end


    def get_text_file
      txt = YAML.load_file('features/pages/host_profile/host_profile_texts.yml')
      txt
    end


  end
end