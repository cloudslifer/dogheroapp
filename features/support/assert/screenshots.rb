require 'open-uri'
require 'capybara-screenshot/rspec'
require 'base64'
require 'capybara/cucumber'
require 'capybara-screenshot/cucumber'

module Screenshots

  @@file_path

  def initialize
    timer_path = Time.now.strftime('%Y_%m_%d')
    @@file_path = "screenshots/#{timer_path}/#{$scenario.name}/"
    FileUtils.mkdir_p(@@file_path) unless File.exist?(@@file_path)

    $file = @@file_path
  end

  def take_screenshot_steps (description, status='passed')
    begin
      $count_steps += 1

      path = "#{@@file_path}/step_#{$count_steps}_#{description}_#{status}.png"
      $appium_driver.screenshot path

      img = encode_64 path
      embed(img, 'image/png;base64', 'SCREENSHOT')
    rescue StandardError
    end
  end

  def take_final_screenshot
    path = "#{@@file_path}/z_final.png"
    $appium_driver.screenshot path

    img = encode_64 path
    img
  end

  def encode_64 file_path
    image = open(file_path, 'rb', &:read)
    encoded_image = Base64.encode64(image)
    encoded_image
  end
end
