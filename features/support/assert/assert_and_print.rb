require 'capybara-screenshot/rspec'
require 'rspec'


module AssertAndPrint include RSpec::Matchers

  def expecting(condition, expected, description)
    if condition == expected
      status = 'passed'
      log_info description
    else
      log_error description
      status = 'failed'
    end

    take_screenshot_steps description, status


    expect(condition).to eql(expected)

  end

  def tracing(description, screenshot = false)
    log_info description
    if screenshot
      take_screenshot_steps description, 'passed'
    end

  end

  def tracing_error(description, screenshot = true)
    log_error description

    unless screenshot
      take_screenshot_steps description, 'failed'
    end


    expect(true).to eql(false)
  end

  private



  def log_warning warning_msg
    msg = "[WARNING]: #{warning_msg}"
    puts msg
  end

  def log_info info_msg
    msg = "[INFO]: #{info_msg}"
    puts msg
  end

  def log_error error_msg
    if ANDROID
      plat = "Android"
    else
      plat = "iOS"
    end

    msg = "[ERRO]: #{error_msg}
          *** Detalhes do erro ***
          Data: #{Time.now.strftime('%d/%m/%Y').to_s}
          Horário: #{Time.now.strftime('%H:%M:%S')}
          Plataforma: #{plat}
          Ambiente: #{TEST_ENVIRONMENT.upcase}"
    $error_log = msg

  end


end