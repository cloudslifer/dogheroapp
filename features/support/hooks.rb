require 'capybara-screenshot/rspec'
require 'base64'
require 'capybara/cucumber'
require 'capybara-screenshot/cucumber'
require 'cucumber'
require 'capybara/cucumber'


SitePrism::Page.class_eval do
  include SpecPages, FindEl, ClickEl, SendKeys, WaitElement, Check, CommonHelpers, KeyCommand, RetryAction, Screenshots, AssertAndPrint, TestData, SwipeHelper, AddressesFile,  CreditCardFile, ApiHostHelper, ApiClientHelper
end


Before do | scenario |

  $scenario = scenario
  $count_steps = 0
end

After do | scenario |
  path = "final_step.png"
  $appium_driver.screenshot path

  img = encode_64 path
  embed(img, 'image/png;base64', 'SCREENSHOT')


  puts $error_log
  $driver.reset
  $driver.close_app
  $driver.launch_app

end

at_exit do
  $driver.quit
end