module ApiClientHelper

  def get_breeds_api_helper
    login_api_helper
    breeds = breeds_api.get_breeds(login_api.get_auth_token)
    breeds
  end

  def assure_one_pet_registered_api_helper
    login_api_helper
    step "acesso meus pets com sucesso --pets-client"
    step "deleto todos os pets --pets-client"
    step "verifico que a quantidade do pet é igual a 0 --pets-client"
    step "cadastro um novo pet --pets-client"
  end

  def cancel_reservation
    tracing "Efetuando o cancelamento da reserva #{$reservation_id} via API"
    login_api_helper
    step "cancelo a minha reserva '#{$reservation_id}' --boarding-cancelation-client"
  end

  def mass_cancelation
    login_api_helper
    step "cancelo todas as minhas reservas --boarding-cancelation-client"
    step "verifico que não há mais reservas em aberto --inbox-client"
  end

  def new_reservation
    login_api_helper
    step "crio uma nova reserva com o herói '#{HOST_ID}' --reservation-client"
    step "verifico que a reserva foi criada com sucesso --reservation-client"
    step "efetuo o pagamento com boleto --payment-client"
    step "verifico que o pagamento foi efetuado com sucesso --payment-client"
  end

  def new_conversation
    login_api_helper
    step "inicio uma nova conversa com o herói '#{HOST_ID}' --conversation-client"
    step "verifico que a conversa foi iniciada com sucesso --conversation-client"
  end

  def login_api_helper
    step "efetuo um login com email '#{CLIENT_EMAIL}' e senha '#{CLIENT_PASSWORD}' --login-client"
    step "verifico que o login foi efetuado com sucesso --login"
  end

  def get_inbox_tab_client tab
    login_api_helper
    step "acesso o inbox no modo '#{"user"}' e na aba '#{tab}' --inbox"
  end
end

