module ApiHostHelper

  def reject_all_reservations
    login_host_api_helper
    step "eu recuso todas as reservas --reservations-host"
  end

  def check_inbox_host_api_helper
    step "que estou no inbox logado com email '#{HOST_EMAIL}' e senha '#{HOST_PASSWORD}' --inbox-client"
  end

  def get_inbox_tab_host_api_helper tab
    login_host_api_helper
    step "acesso o inbox no modo '#{"hero"}' e na aba '#{tab}' --inbox"
  end

  def login_host_api_helper
    step "efetuo um login com email '#{HOST_EMAIL}' e senha '#{HOST_PASSWORD}' --login-host"
    step "verifico que o login foi efetuado com sucesso --login"
  end

end

