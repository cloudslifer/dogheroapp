module AddressesFile


  def load_address_file
    addresses_file = YAML.load_file('features/support/data/test/files/addresses/addresses.yml')
    addresses_file
  end


  def get_address_from_yml
    addresses = load_address_file['addresses'].values
    addresses
  end

  def get_specific_address_type_from_yml address_type
    address = load_address_file['addresses'][address_type.downcase]
    address
  end


end
