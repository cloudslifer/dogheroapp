module CreditCardFile


  def load_credit_card_file
    file = YAML.load_file('features/support/data/test/files/credit_card/credit_card.yml')
    file
  end



  def get_credit_card_from_yml card_brand
    file = load_credit_card_file['credit_card'].to_hash

    if card_brand == 'random'
      file = file[sort_holder].split(',').sample
    else
      file = file[card_brand].split(',').sample
    end
    file
  end

  def sort_holder
    $amex = false
    holders = load_credit_card_file['credit_card']
    holders = holders.keys.sample
    if holders == 'amex'
      $amex = true
    end
    holders
  end

  def get_credit_card_cvv
    if $amex
      rand(1111...9999).to_s
    else
      rand(111...999).to_s
    end
  end


end
