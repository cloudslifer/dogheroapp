
module TestData


  def default_password password
    if password == 'default'
      password = 'Dh1234@'
      password
    else
      password
    end
  end

  def random_data (text, type)
    type.downcase!
    text.downcase!
    if text == 'random'
      case type
      when 'name'
        text = Faker::Name.first_name
      when 'last_name'
        text = Faker::Name.last_name
      when 'full_name'
        text = Faker::Name.name
      when 'dog_name'
        text =  Faker::Games::Pokemon.name
      when 'verb'
        text = Faker::Verb.base
      when 'cpf'
        text = Faker::IDNumber.brazilian_citizen_number
      when 'day'
        text = rand(01...29).to_s
      when 'month'
        text = rand(01...12).to_s
      when 'full_year_past'
        text = rand(1960...1996).to_s
      when 'full_year_future'
        text = rand(2019...2099).to_s
      when 'full_year_all'
        text = rand(1960...2099).to_s
      when 'year_past'
        text = rand(60...96).to_s
      when 'year_future'
        text = rand(19...99).to_s
      when 'year_all'
        text = rand(60...99).to_s
      when 'paragraph'
        text =  Faker::Lorem.paragraph
      when 'phone_number'
        text = rand(1111111111...9999999999).to_s
      when 'address_number'
        text = rand(11111...99999).to_s
      when 'ddd'
        text = rand(10...99).to_s
      when 'email'
        text = 'qa_floquinho_app_' + Faker::Number.between(1, 10000000).to_s + '@mailinator.com'
      when 'address'
        text = get_address_from_yml.sample
      when 'credit_card'
        text = get_credit_card_from_yml 'random'
      when 'cvv'
        text = get_credit_card_cvv
      end
    elsif type == 'address'
      text = get_specific_address_type_from_yml text
    elsif type == 'credit_card'
      text = get_credit_card_from_yml text
    end
    text
  end

  def random_boolean
    [true, false].sample
  end
end
