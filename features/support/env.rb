require 'rubygems'
require 'rspec'
require 'selenium-webdriver'
require 'capybara/cucumber'
require 'pry'
require 'capybara-screenshot/rspec'
require 'site_prism'
require 'base64'
require 'appium_capybara'
require 'faker'

Dir[File.join(File.dirname(__FILE__), 'spec_helper/*.rb')].sort.each {|file| require file}
Dir[File.join(File.dirname(__FILE__), 'assert/*.rb')].sort.each {|file| require file}
Dir[File.join(File.dirname(__FILE__), 'elements/**/*.rb')].sort.each {|file| require file}
Dir[File.join(File.dirname(__FILE__), 'data/**/*.rb')].sort.each {|file| require file}
Dir[File.join(File.dirname(__FILE__), 'utils/**/*.rb')].sort.each {|file| require file}
Dir[File.join(File.dirname(__FILE__), 'helpers/*.rb')].sort.each {|file| require file}
Dir[File.join(File.dirname(__FILE__), 'api/*.rb')].sort.each {|file| require file}

PATH_API_REST = 'doghero_api/'
require_relative '../../floquinho_api/lib/dogheroapi/dogheroapi'


COUNTRY ||= ENV['COUNTRY']

ENV['IOS'].nil? ? IOS = false : IOS = true
ENV['ANDROID'].nil? ? ANDROID = false : ANDROID = true
ENV['REMOTE'].nil? ? REMOTE = false : REMOTE = true
ENV['SIMULATORS'].nil? ? SIMULATORS = false : SIMULATORS = true


ENV['TEST_ENVIRONMENT'] ? TEST_ENVIRONMENT = ENV['TEST_ENVIRONMENT'] : 'não informado'

CLIENT_EMAIL = 'app_qa@mailinator.com'
CLIENT_PASSWORD = 'suporte123'

HOST_EMAIL = 'qa_app_host@mailinator.com'
HOST_PASSWORD = 'suporte123'
HOST_ID = '315947'


def load_support_file file_name
  begin
    caps_file = YAML.load_file("features/support/#{file_name}")
    caps_file
  rescue StandardError => e
    raise "Erro ao carregar o arquivo #{file_name}. Erro: #{e.message}"
  end
end


def set_local_caps
  caps_file = load_support_file 'capabilities.yml'
  caps = {}

  caps.merge! caps_file['local']['generic']

  platform = ENV['PLATFORM']

  IOS ? platform_name = 'ios' : platform_name = 'android'

  caps.merge! caps_file['local'][platform_name]['generic']


  if platform.nil? || platform == platform_name
    caps.merge! caps_file['local'][platform_name][platform_name]
  else
    caps.merge! caps_file['local'][platform_name][platform]
  end

  caps
end

def set_simulators_caps
  caps = {}
=begin
  caps['deviceName'] = 'iPhone 8 Plus'
  caps['version'] = '13.3'
  caps['platformName'] = 'iOS'
  caps['app'] = "/var/lib/jenkins/doghero/DogHero.app"
  caps['automationName'] = 'XCUITest'
=end

  caps['deviceName'] = 'emulator-5554'
  caps['version'] = '10.0'
  caps['platformName'] = 'ANDROID'
  caps['appActivity'] = 'br.com.doghero.astro.SplashscreenActivity'
  caps['appPackage'] = 'br.com.doghero.astro'
  caps['language'] = 'pt'
  caps['locale'] = 'br'
  caps['newCommandTimeout'] = 10000

  caps
end

def set_remote_caps
  caps_file = load_support_file 'capabilities.yml'
  caps = {}

  IOS ? platform_name = 'ios' : platform_name = 'android'

  project_caps = {}
  project_caps['project'] = platform_name
  project_caps['name'] = "#{ENV['TEST_NAME']}_"
  project_caps['build'] = "#{ENV['BUILD_NUMBER']}_#{ENV['JOB_NAME']}"

  devices_file = load_support_file 'devices.yml'

  device_caps = set_device devices_file[platform_name]

  caps.merge! project_caps

  caps.merge! device_caps

  caps.merge! caps_file['remote'][platform_name]

  caps.merge! caps_file['remote']['generic']

  caps

end

def set_device file
  specific_device = ENV['specific_device']

  if specific_device.nil?
    specific_device = file.keys.sample
  end

  file[specific_device]
end

def set_driver
  if REMOTE

    if SIMULATORS
      use_caps = set_simulators_caps
      puts "SIMULATOR CAPS: #{use_caps}"
      use_url = "http://localhost:4445/wd/hub"
    else
      username = 'doghero1'
      access_key = 'kwxVmpWftCTAiaUshEQE'

      use_caps = set_remote_caps
      use_url = "http://#{username}:#{access_key}@hub-cloud.browserstack.com/wd/hub"

    end
  else
    use_caps =  set_local_caps
    use_url = "http://127.0.0.1:4723/wd/hub"
  end


  driver = Appium::Driver.new({
                                  'caps' => use_caps,
                                  'appium_lib' => {
                                      :server_url => use_url
                                  }}, true)

  driver
end

def start_appium
  begin
    $appium_driver = set_driver
    $appium_driver.start_driver
  rescue StandardError => e
    puts "MSG ERROR: #{e.message}"
    sleep 5
    puts "Retrying - Launching Appium"
    $appium_driver.start_driver
  end
end

$driver = start_appium


World(SpecPages, ClickEl, SendKeys, FindEl, WaitElement, Check, KeyCommand, RetryAction, Screenshots, AssertAndPrint, CommonHelpers, TestData, AddressesFile,  CreditCardFile, SwipeHelper, ApiClientHelper, ApiHostHelper)