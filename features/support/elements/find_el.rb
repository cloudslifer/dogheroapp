module FindEl

  @page_name
  @description
  @finder
  @el_locator

  @@tries = 2


  def define_finders(el_name)
    discover = DiscoverElement.new
    discover.discover(el_name)

    @description = discover.description
    @finder = discover.finder
    @el_locator = discover.element
  end

  def find_el(el_name)
    retry_finder el_name do
      wait_until_el_displayed @finder, @el_locator
    end
  end


  def find_el_exists(el_name)
    res = retry_finder_exists el_name do
      check_is_displayed @finder, @el_locator
    end

    res ? true : false
  end

  def find_all_els(el_name)
    retry_finder el_name do
      wait_until_el_list_displayed @finder, @el_locator
    end

  end

  def get_random_el(el_name)
    els = find_all_els el_name
    els.sample
  end


  def find_child_el(element, el_name)
    retry_finder el_name do
      wait_until_child_el_displayed element, @finder, @el_locator
    end
  end

  def find_exists_fast el_name
    define_finders el_name
    begin
      using_wait_time 2 do
        element.find_element(@finder, @el_locator)
      end
      true
    rescue StandardError
      false
    end

  end

  def find_child_el_exists(element, el_name)
    define_finders el_name

    begin
      using_wait_time 1 do
        element.find_element(@finder, @el_locator)
      end
      true
    rescue StandardError
      false
    end
  end

  def find_child_el_list(element, el_name)
    retry_finder el_name do
      wait_until_child_el_list_displayed element, @finder, @el_locator
    end
  end

  def check_exists el_result
    if el_result.nil?
      false
    else
      el_result.size>0 ? true : false
    end
  end


  def retry_finder el_name
    define_finders el_name
    retries = 0

    begin
      yield
    rescue StandardError => e
      retries += 1
      if retries < @@tries
        puts "Procurando elemento:   #{el_name}"
        puts "#{Time.now}"
        retry
      else
        find_el_log_error(el_name,e.message)
      end

    end

  end

  def retry_finder_exists el_name
    define_finders el_name
    retries = 0
    begin
      yield
    rescue StandardError
      puts "Verificando se elemento #{el_name} existe"
      puts "#{Time.now}"
      retries += 1
      if retries < @@tries
        retry
      else
        nil
      end
    end

  end


  def find_el_log_error el_name, e
    raise "Elemento não encontrado: #{el_name}. Locator: #{@el_locator}. Descrição: #{@description}. Exceção: #{e}"
  end



end