module WaitElement

  def wait_until_el_displayed(finder, el)
    wait_els do
      $driver.find_element(finder, el).displayed?
    end
    $driver.find_element(finder, el)
  end

  def check_is_displayed(finder, el)
    wait_els do
      $driver.find_element(finder, el).displayed?
    end
    $driver.find_element(finder, el)
  end

  def wait_until_child_el_displayed(element, finder, el)
    wait_els do
      element.find_element(finder, el).displayed?
    end
    element.find_element(finder, el)

  end

  def wait_until_child_el_list_displayed(element, finder, el)
    wait_els do
      element.find_elements(finder, el).size > 0
    end
    element.find_elements(finder, el)
  end

  def wait_until_el_list_displayed(finder, el)
    wait_els do
      $driver.find_elements(finder, el).size > 0
    end
    $driver.find_elements(finder, el)
  end


  def wait_els
    options = {}
    options[:timeout] = 7
    options[:interval] = 1
    wait = Selenium::WebDriver::Wait.new(options)
    bool = wait.until {
      yield
    }

    bool.nil? ? false : bool
  end


end