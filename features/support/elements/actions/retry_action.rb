module RetryAction


  def retry_action action_name, el_name
    retries = 0

    begin
      yield
    rescue StandardError => e
      if retries <=1
        retries += 1
        retry
      else
        raise "Erro ao #{action_name} no elemento #{el_name}. Erro: #{e.message} "
      end

    end
  end

  def assure_el_is_visible el
    unless el.nil?
      unless el.displayed?
        swiper :down
      end
    end

  end


end
