module ClickEl

  def click_el(el)
    element = find_el(el)

    retry_action "clique", el do
      assure_el_is_visible element
      element.click
    end

  end

  def click_random_el el
    elements = find_all_els(el)
    retry_action "clique", el do

      el = elements.sample
      assure_el_is_visible el
      el.click
    end
  end

  def click_solved_el el
    assure_el_is_visible el
    el.click

  end

  def click_random_enabled_el el
    elements = find_all_els(el)

    retry_action "clique", el do

      elements.each do | element|
        if element.enabled?

          assure_el_is_visible element
          element.click
          break
        end
      end
    end
  end


  def click_range_position_el(el, position)
    elements = find_all_els(el)

    retry_action "clique", el do

      assure_el_is_visible elements[0]
      elements[rand(position...elements.size)].click
    end
  end

  def click_position_el(el, position)
    elements = find_all_els(el)

    retry_action "clique", el do

      assure_el_is_visible elements[0]
      elements[position].click
    end

  end


  def tap_el(el)
    element = find_el(el)

    retry_action "clique", el do

      assure_el_is_visible element
      Appium::TouchAction.new.tap(element: element, x:0.5, y:0.5).perform
    end

  end

  def tap_position_el(el, position)
    elements = find_all_els(el)

    retry_action "clique", el do

      assure_el_is_visible elements[0]
      Appium::TouchAction.new.tap(element: elements[position], x:0.5, y:0.5).perform
    end

  end

  def tap_solved_el(element)
    assure_el_is_visible element
    Appium::TouchAction.new.tap(element: element, x:0.5, y:0.5).perform
  end

end