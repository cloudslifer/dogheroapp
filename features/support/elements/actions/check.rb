module Check

  def is_visible_el el
    find_el_exists el
  end

  def is_visible_list_el (el)
    elements = find_all_els el
    if elements.size > 0
      true
    else
      false
    end
  end

  def is_checked_el el
    retry_action("verifica se checkbox está checada", el) do
      element = find_el(el)
      assure_el_is_visible element
      ## todo
      # implementar checked
    end
  end

  def get_text_el el
    retry_action("receber text", el) do
      element = find_el(el)
      assure_el_is_visible element
      element.text.strip
    end
  end

  def get_text_solved_el el, el_reference
    retry_action("receber text", el_reference) do
      el.text.strip
    end
  end

  def has_text_solved_el el, reference
    txt = ''
    retry_action("receber text", reference) do
      assure_el_is_visible el
      txt = el.text
    end

    txt.empty? ? false : true
  end

  def has_text_el el
    txt = ''
    retry_action("receber text", el) do
      element = find_el(el)
      assure_el_is_visible element
      txt = element.text
    end

    txt.empty? ? false : true
  end

  def has_text_child_el element, el
    txt = ''
    retry_action("receber text", el) do
      ele = find_child_el element, el
      assure_el_is_visible ele
      txt = ele.text
    end

    txt.empty? ? false : true
  end

  def check_page_has_text text
    begin
      $appium_driver.get_source.include?(text)
      true
    rescue Selenium::WebDriver::Error::NoSuchElementError
      false
    end
  end
end