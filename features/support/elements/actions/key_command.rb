module KeyCommand

  def press_key_code(value, el=nil)
    if ANDROID
      case value
      when 'enter'
        value = 66
      when 'numpad_enter'
        value = 160
      when 'search'
        value = 84
      when 'escape'
        value = 111
      end

      $driver.press_keycode value
    end
  end

end