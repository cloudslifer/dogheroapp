module SwipeHelper

  def swiper(direction)
    get_window

    begin
      dir = direction.to_s.downcase

      case dir
      when 'down'
        Appium::TouchAction.new.swipe(start_x: @start_horizontal_x , start_y: @start_horizontal_y, end_x: @end_horizontal_x, duration:3000).perform
      when 'up'
        Appium::TouchAction.new.swipe(start_x: @start_vertical_x , start_y: @end_vertical_y, end_y: @start_vertical_y, duration:1500).perform
      when 'right'
        Appium::TouchAction.new.swipe(start_x: @start_horizontal_x , start_y: @start_horizontal_y, end_y: @end_horizontal_x, duration:1500).perform
      when 'left'
        Appium::TouchAction.new.swipe(start_y: @start_horizontal_y, end_x: @start_horizontal_x, end_y: @start_horizontal_y, duration:1500).perform
      when 'down_android'
        ANDROID ? Appium::TouchAction.new.swipe(start_x: @start_horizontal_x , start_y: @start_horizontal_y, end_x: @end_horizontal_x, duration:3000).perform : nil
      end
    rescue Selenium::WebDriver::Error::ElementNotInteractableError, Selenium::WebDriver::Error::UnknownError
      sleep 2
      retry
    end
  end

  def get_window
    @window_size = $appium_driver.window_size

    @start_vertical_y = @window_size.height * 0.80
    @end_vertical_y = @window_size.height * 0.21
    @start_vertical_x = @window_size.width / 2.1

    @start_horizontal_x = @window_size.width * 0.9
    @end_horizontal_x = @window_size.width * 0.9
    @start_horizontal_y = @window_size.height / 2.1
  end


end
