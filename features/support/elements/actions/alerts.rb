module Alerts

  def accept_alert
    if IOS
      $appium_driver.alert_accept
    else

    end
  end

  def dismiss_alert
    if IOS
      $appium_driver.alert_dismiss
    else

    end
  end
end

