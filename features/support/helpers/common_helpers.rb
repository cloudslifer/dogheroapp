module CommonHelpers

  def random_bool
    [true, false].sample
  end

  def assert_texts_from_file file, array_texts
    IOS ? plat = 'ios' : plat = 'android'

    assertion = {}
    checker assertion do
      array_texts.each do | txt |
        begin
          assertion[txt] = check_page_has_text(file[txt]['all'][COUNTRY])
        rescue NoMethodError
          begin
            assertion[txt] = check_page_has_text(file[txt][plat][COUNTRY])
          rescue NoMethodError
            raise "Erro! Verifique se a chave #{txt} existe no arquivo YML"
          end
        end
      end
    end
  end

  def assert_texts_from_elements elements
    assertion = {}
    checker assertion do
      elements.each do | el |
        assertion[el] = has_text_el(el)
      end
    end
  end

  def assert_texts_from_solved_elements elements
    assertion = {}
    checker assertion do
      elements.each do | el |
        assertion[el] = has_text_solved_el(el, 'resultados da busca')
        swiper :up
      end
    end
  end


  def assert_texts_from_child_elements element_father, elements
    assertion = {}
    checker assertion do
      elements.each do | el |
        assertion[el] = has_text_child_el(element_father, el)
      end
    end
  end

  def checker assertion
    yield

    failed_assertion = {}
    passed = true

    assertion.each do |key, value|
      if value == false
        passed = false
        failed_assertion[key] = value

      end
    end

    unless passed
      log_error "Erro na checagem dos textos: #{failed_assertion}"
    end

    passed
  end

  def press_back
    $appium_driver.back
  end
end