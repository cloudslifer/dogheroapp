module SpecPages

  def onboarding
    SpecPages::Onboarding.new
  end

  def device_controllers_camera
    SpecPages::DeviceControllersCamera.new
  end

  def device_controllers_keyboard
    SpecPages::DeviceControllersKeyboard.new
  end

  def home
    SpecPages::Home.new
  end

  def host_intake
    SpecPages::HostIntake.new
  end

  def host_profile
    SpecPages::HostProfile.new
  end

  def language
    SpecPages::Language.new
  end
  def login
    SpecPages::Login.new
  end

  def my_heroes
    SpecPages::MyHeroes.new
  end

  def my_pets
    SpecPages::MyPets.new
  end

  def my_addresses
    SpecPages::MyAddresses.new
  end

  def my_credit_cards
    SpecPages::MyCreditCards.new
  end

  def location
    SpecPages::Location.new
  end

  def menu
    SpecPages::Menu.new
  end

  def pre_encontro
    SpecPages::PreEncontro.new
  end

  def register
    SpecPages::Register.new
  end

  def search
    SpecPages::Search.new
  end

  def conversation
    SpecPages::Conversation.new
  end

  def reservation
    SpecPages::Reservation.new
  end

  def payment
    SpecPages::Payment.new
  end

end