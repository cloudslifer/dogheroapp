Então("sou direcionado para a tela de contato --pre-encontro") do
  pre_encontro.close_advise
  expecting(pre_encontro.is_pre_encontro_page, true, 'Sou direcionado para a tela de contato')
end
