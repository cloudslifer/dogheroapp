Quando("preencho meu cadastro corretamente --host-intake") do
  host_intake.wanna_be_host
  host_intake.choose_basic_info
  host_intake.choose_profile_picture
  host_intake.fill_basic_info
end