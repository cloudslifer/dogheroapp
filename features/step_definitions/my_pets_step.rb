Quando("eu cadastro inicialmente um pet com nome {string} sexo {string} raça {string} idade {string} tamanho {string} foto {string} --my-pets") do |name, sex, breed, age, size, picture|
  step "informo que tenho um pet --onboarding"
  breed_list = get_breeds_api_helper
  my_pets.fill_dog(name, sex, breed, breed_list, age, size, picture)
end

Entao("eu estou na tela de 'Meus Pets' com uma localização informada e logado --my-pets") do | table |
  step("estou na tela inicial com uma localização informada e logado --home", table)
  step "abro o menu --home"
  step "vou para a tela de 'meus pets' --menu"

  expecting(my_pets.is_my_pets, true, "Verifica que está na tela de meus pets")
end

Quando("eu cadastro um pet com nome {string} sexo {string} raça {string} idade {string} tamanho {string} foto {string} --my-pets") do |name, sex, breed, age, size, picture|
  my_pets.add_pet
  breed_list = get_breeds_api_helper
  my_pets.fill_dog(name, sex, breed, breed_list, age, size, picture)
end

Então("meu pet é adicionado com sucesso --my-pets") do
  expecting(my_pets.check_added_pet, true, "Verifica que a quantidade de pets foi atualizada para mais")
end

Quando("eu deleto um dos pets --my-pets") do
  my_pets.delete_pet
end

Então("o pet é removido com sucesso --my-pets") do
  expecting(my_pets.check_deleted_pet, true, "Verifica que o número de pets foi atualizado para menos")
end

Entao("uma mensagem de sucesso é apresentada solicitando um cadastro na doghero --my-pets") do
  expecting(my_pets.success_pet_registration, true, 'Pet cadastrado com sucesso')
end