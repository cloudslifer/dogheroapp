Entao("verifico que estou na tela de conversa --conversation") do
  expecting(conversation.is_conversation, true, "Verifica que está na tela de conversa")
end

Dado("estou na tela de conversa com um anfitrião de testes e serviço de boarding --conversation") do | table |
  step("vou para o perfil do anfitrião de testes --my-heroes", table)
  step "valido que estou no perfil do anfitrião com serviço de boarding --host-profile"
  step "clico em iniciar uma conversa --host-profile"
  step "verifico que estou na tela de conversa --conversation"
  step "verifico que o serviço de boarding está selecionado --conversation"
end

Então("verifico que o serviço de boarding está selecionado --conversation") do
  expecting(conversation.is_boarding_active, true, "Verifica que o serviço de boarding está selecionado")
end

Quando("altero a data de hospedagem --conversation") do
  conversation.set_days
end

Então("verifico se a quantidade de noites apresentada está conforme o que foi escolhido --conversation") do
  expecting(conversation.check_nights_qty, true, "Verifica a quantidade de noites apresentada")
  expecting(conversation.check_price, true, "Verifica que o preço é apresentado")
end

Quando("escolho o período de checkin para {string} e o período de checkout para {string} --conversation") do | checkin, checkout |
  conversation.checkin_period checkin
  conversation.checkout_period checkout
end

Entao("verifico se meio período está sendo apresentado corretamente --conversation") do
  expecting(conversation.check_half_period, true, "Verifica o meio período")
end

Quando("adiciono um novo pet --conversation") do
  conversation.add_new_pet
  breed_list = get_breeds_api_helper
  my_pets.fill_dog('random', 'random', 'random', breed_list, 'random', 'random', 'no')
end

Então("o novo pet é apresentado com sucesso --conversation") do
  expecting(conversation.check_pet_added, true, "Verifica que o pet foi adicionado com sucesso")
end

Quando("habilito um pet existente --conversation") do
  conversation.switch_pets 'enable'
  @@action = 'enable'
end

Quando("desabilito um pet existente --conversation") do
  conversation.switch_pets 'disable'
  @@action = 'disable'
end


Entao("o valor total é alterado com sucesso --conversation") do
  expecting(conversation.compare_prices(@@action), true, "Verifica se preço foi alterado")
end

Quando("seleciono a opção de pré-encontro {string} --conversation") do | option|
  conversation.pre_meeting option
end

Quando("eu envio uma mensagem {string} --conversation") do | msg|
  conversation.write_msg msg
  conversation.send_msg
end

Então("uma tela de sucesso é apresentada --conversation") do
  expecting(conversation.is_msg_sent, true, "Verifica que a mensagem foi enviada com sucesso")
end