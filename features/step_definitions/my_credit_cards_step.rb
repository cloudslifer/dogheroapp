Entao("verifico se há cartões adicionados --my-credit-cards") do
  my_credit_cards.has_credit_cards
end

Quando("clico para adicionar um novo cartão --my-credit-cards") do
  my_credit_cards.add_new_credit_card
end

Quando("adiciono um novo cartão da bandeira {string} --my-credit-cards") do | bandeira |
  my_credit_cards.fill_new_credit_card bandeira
  my_credit_cards.save_new_credit_card
end

Então("verifico que o cartão foi adicionado com sucesso --my-credit-cards") do
  expecting(my_credit_cards.check_credit_card_added, true, "verifica que a quantidade de cartões é maior")
end

Quando("seleciono um cartão aleatório --my-credit-cards") do
  my_credit_cards.select_random_credit_card
end
