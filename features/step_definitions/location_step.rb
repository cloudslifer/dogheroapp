Dado("que acesso o app e pulo as introduções utilizando uma localidade desejada --location") do |table|
  step "clico em continuar na sessão de boas vindas --onboarding"
  step "escolho o idioma default --onboarding"
  step "pulo o onboarding --onboarding"

  table.hashes.each do |row|
    step "escolho um endereço '#{row[:address]}' --location"
  end
end

Dado("escolho um endereço {string} --location") do | address |
  location.set_address address
end

Dado("uso minha localização atual --location") do
  location.use_current_location
end