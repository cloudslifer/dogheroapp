Dado("clico em continuar na sessão de boas vindas --onboarding") do
  onboarding.continue_welcome
end

Dado("que acesso o app --onboarding") do
  onboarding.continue_welcome
  step "escolho o idioma default --language"
end

E("pulo o onboarding --onboarding") do
  onboarding.skip_onboarding
end

E("informo que tenho um pet --onboarding") do
  onboarding.have_dog
end

E("pulo o onboarding e utilizo uma localidade desejada --onboarding") do | table|
  onboarding.skip_onboarding

  table.hashes.each do |row|
    step "escolho um endereço '#{row[:address]}' --location"
  end
end