Dado("estou na tela de um anfitrião aleatório com a localização informada --host-profile") do | table |
  step("estou na tela inicial com uma localização informada --home", table)
  step "entro no perfil do anfitrião"

end

Dado("estou no perfil de um anfitrião de testes --host-profile") do | table |
  step("vou para o perfil do anfitrião de testes --my-heroes", table)
  step "valido que estou no perfil do anfitrião --host-profile"
end


Dado("favorito um anfitrião em seu perfil com uma localização informada e logado --host-profile") do | table |

  step("estou na tela inicial com uma localização informada e logado --home", table)
  step "entro no perfil do anfitrião"
  step "favorito o anfitrião"
end

Quando("favorito o anfitrião") do
  host_profile.favorite_host
end

E("entro no perfil do anfitrião") do
  step "clico na opção de busca por anfitrião --home"
  step "valido que estou na tela de busca --search"
  step "clico no perfil de um anfitrião aleatório --search"
  step "valido que estou no perfil do anfitrião --host-profile"
end

Quando("clico em iniciar uma conversa --host-profile") do
  host_profile.go_to_conversation
end

Quando("clico em efetuar uma reserva --host-profile") do
  host_profile.go_to_reservation
end

Quando("clico na imagem da capa do anfitrião --host-profile") do
  host_profile.open_gallery
end

Entao("valido a galeria de fotos é apresentada com sucesso --host-profile") do
  expecting(host_profile.check_gallery, true, "Verifico que a galeria de imagens é apresentada com sucesso")
end

E("que os dados apresentados na busca conferem com os de seu perfil --host-profile") do
  expecting(host_profile.check_data_from_search, true, "Verifica que os dados referentes a 'Nome' e 'Preço' do anfitrião estão de acordo com a search")
end

E("verifico que o anfitrião tem o serviço de boarding --host-profile") do
  expecting(host_profile.has_boarding, true, "Verifica que o serviço de boarding é encontrado")
end
E("que é possivel alterar as imagens ao deslizar --host-profile") do
  expecting(host_profile.change_picture, true, "Verifica que as imagens são alteradas ao deslizar")
end

Entao("valido que estou no perfil do anfitrião --host-profile") do
  expecting(host_profile.is_host_profile, true, "Verifica que está no perfil do anfitrião")
end

Entao("valido que estou no perfil do anfitrião com serviço de boarding --host-profile") do
  step "valido que estou no perfil do anfitrião --host-profile"
  step "verifico que o anfitrião tem o serviço de boarding --host-profile"
end