Dado("estou na tela de busca com uma localização informada --search") do | table |
  step("estou na tela inicial com uma localização informada --home", table)
  step "clico na opção de busca por anfitrião --home"
  step "valido que estou na tela de busca --search"
end


Dado("estou na tela de busca com uma localização informada e logado --search") do | table |
  step("estou na tela inicial com uma localização informada e logado --home", table)
  step "clico na opção de busca por anfitrião --home"
  step "valido que estou na tela de busca --search"
end

Quando("vou para a tela de meus heróis --search") do
  search.exit_profile
  step "vou para a tela de meus heróis --home"
end


Dado("favorito um anfitrião na tela de busca com uma localização informada e logado --search") do | table |
  step("estou na tela inicial com uma localização informada e logado --home", table)
  step "clico na opção de busca por anfitrião --home"
  step "valido que estou na tela de busca --search"
  step "favorito um anfitrião --search"

end

Quando("favorito um anfitrião --search") do
  search.favorite_hero
end

Quando("clico no perfil de um anfitrião aleatório --search") do
  search.go_to_random_host_profile
end

Entao("valido que estou na tela de busca --search") do
  expecting(search.is_search, true, 'Verifica que está na tela de busca')
end

Quando("clico para alterar o endereço --search") do | table |
  search.change_address
  table.hashes.each do | row|
    step "escolho um endereço '#{row[:address]}' --location"
  end
end

Quando("altero a data de busca --search") do
  search.change_date
end

Quando("altero a data de busca nos filtros adicionais --search") do
  step "abro os filtros adicionais --search"
  search.change_date_filter
  if ANDROID
    search.apply_filter
  end
end

Quando("seleciono filtros adicionais --search") do
  step "abro os filtros adicionais --search"
  search.choose_filters
end

E("abro os filtros adicionais --search") do
  search.open_filters
end

E("que o conteúdo das informações não está vazio --search") do
  expecting(search.check_result, true, "Verifica o conteúdo dos campos 'Nome', 'Título', 'Localização', e 'Preço'")
end

Entao("verifico que os resultados da busca foram retornados com sucesso --search") do
  expecting(search.is_valid_result, true, 'Verifica que a busca retornou resultados válidos')
end

Quando("abro o mapa do google --search") do
  search.open_map
end

Entao("o mapa é apresentado corretamente --search") do
  expecting(search.check_map, true, 'Verifica que o mapa é apresentado com sucesso')
end

E("abre o perfil de um anfitrião recomendado através do map --search") do
  search.open_host_from_map
end

E("abro o perfil de um anfitrião --search") do
  search.open_host_profile
end

Quando("valido a apresentação de mais resultados --search") do
  search.swipe_pagination
end
