Quando("efetuo meu cadastro com email {string} e senha {string} --register") do | email, password|
  register.register_user email, password
end

Entao("verifico que um alerta de erro foi apresentado ao tentar efetuar o cadastro --register") do
  expecting(register.invalid_register, true, 'Alerta de cadastro inválido é apresentado')
end