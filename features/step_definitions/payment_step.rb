Dado("estou na tela de pagamento com um anfitrião de testes e serviço de boarding --payment-boarding") do | table |
  step("estou na tela de reserva com um anfitrião de testes e serviço de boarding --reservation", table)
  step "preencho todos os dados necessários e efetuo uma reserva --reservation"
  step "verifico que estou na tela de pagamento de boarding --payment-boarding"
end

Entao("verifico que estou na tela de pagamento de boarding --payment-boarding") do
  expecting(payment.is_payment, true, "Verifica que está na tela de pagamento de boarding")
end

Quando("clico para adicionar um novo endereço --payment-boarding") do
  payment.select_address
end

Quando("seleciono o pagamento com cartão de crédito --payment-boarding") do
  payment.choose_payment_credit_card
end

Quando("seleciono meu parcelamento --payment-boarding") do
  payment.choose_installments
end

Quando("seleciono o pagamento com boleto --payment-boarding") do
  payment.choose_payment_boleto
end

Quando("adiciono um novo cartão de crédito da bandeira {string} --payment-boarding") do | bandeira |
  payment.select_credit_card
  step "verifico se há cartões adicionados --my-credit-cards"
  step "clico para adicionar um novo cartão --my-credit-cards"
  step("adiciono um novo cartão da bandeira {string} --my-credit-cards", bandeira)
  step "verifico que o cartão foi adicionado com sucesso --my-credit-cards"
  step "seleciono um cartão aleatório --my-credit-cards"
end

Quando("seleciono um cartão aleatório --payment-boarding") do
  payment.select_credit_card
  step "verifico se há cartões adicionados --my-credit-cards"
  step "seleciono um cartão aleatório --my-credit-cards"
end

Quando("adiciono um novo endereço {string} --payment-boarding") do | address |
  payment.select_address
  step "verifico se há endereços cadastrados --my-addresses"
  step "clico para adicionar um novo endereço --my-addresses"
  step("adiciono um novo endereço {string} --my-addresses", address)
  step "verifico que o endereço foi adicionado com sucesso --my-addresses"
  step "seleciono um endereço aleatório --my-addresses"
end

Quando("seleciono um endereço aleatório --payment-boarding") do
  payment.select_address
  step "verifico se há endereços cadastrados --my-addresses"
  step "seleciono um endereço aleatório --my-addresses"
end

Quando("altero meu nome para {string}, cpf para {string} e celular para {string} --payment-boarding") do | name, cpf, phone|
  payment.change_personal_data name, cpf, phone
end

Quando("finalizo o meu pagamento --payment-boarding") do
  payment.finish_reservation
end

Então("verifico que minha reserva foi solicitada com sucesso --payment-boarding") do
  expecting(payment.success_msg, true, "verifica mensagem de sucesso ao efetuar pagamento")
end
