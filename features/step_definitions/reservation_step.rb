Entao("verifico que estou na tela de reserva --reservation") do
  expecting(reservation.is_reservation, true, "Verifica que está na tela de reserva")
end

Dado("estou na tela de reserva com um anfitrião de testes e serviço de boarding --reservation") do | table |
  step("vou para o perfil do anfitrião de testes --my-heroes", table)
  step "valido que estou no perfil do anfitrião com serviço de boarding --host-profile"
  step "clico em efetuar uma reserva --host-profile"
  step "verifico que estou na tela de reserva --reservation"
  step "verifico que o serviço de boarding está selecionado --reservation"
end

Quando("altero a data de hospedagem --reservation") do
  reservation.set_days
end

Então("verifico que o serviço de boarding está selecionado --reservation") do
  expecting(reservation.is_boarding_active, true, "Verifica que o serviço de boarding está selecionado")
end

Então("verifico se a quantidade de noites apresentada está conforme o que foi escolhido --reservation") do
  expecting(reservation.check_nights_qty, true, "Verifica a quantidade de noites apresentada")
  expecting(reservation.check_price, true, "Verifica que o preço é apresentado")
end

Quando("escolho o período de checkin para {string} e o período de checkout para {string} --reservation") do | checkin, checkout |
  reservation.checkin_period checkin
  reservation.checkout_period checkout
end

Entao("verifico se meio período está sendo apresentado corretamente --reservation") do
  expecting(reservation.check_half_period, true, "Verifica o meio período")
end

Quando("adiciono um novo pet --reservation") do
  reservation.add_new_pet
  breed_list = get_breeds_api_helper
  my_pets.fill_dog('random', 'random', 'random', breed_list, 'random', 'random', 'no')
end

Então("o novo pet é apresentado com sucesso --reservation") do
  expecting(reservation.check_pet_added, true, "Verifica que o pet foi adicionado com sucesso")
end

Quando("habilito um pet existente --reservation") do
  reservation.switch_pets 'enable'
  @@action = 'enable'
end

Quando("desabilito um pet existente --reservation") do
  reservation.switch_pets 'disable'
  @@action = 'disable'
end


Entao("o valor total é alterado com sucesso --reservation") do
  expecting(reservation.compare_prices(@@action), true, "Verifica se preço foi alterado")
end

Quando("preencho todos os dados necessários e efetuo uma reserva --reservation") do
  step "altero a data de hospedagem --reservation"

  step"escolho o período de checkin para '#{'random'}' e o período de checkout para '#{'random'}' --reservation"

  step "efetuo uma reserva --reservation"
end

Quando("efetuo uma reserva --reservation") do
  reservation.click_reservation
end
