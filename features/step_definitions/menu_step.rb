E("vou para a tela de login --menu") do
  step 'abro o menu --home'
  step 'escolho a opção de login --menu'
end

Dado("escolho a opção de login --menu") do
  menu.go_to_login
end

E("acesso a opção 'quero ser um anfitrião' --menu") do
  menu.wanna_be_host
end

Quando("vou para a tela de 'meus pets' --menu") do
  menu.go_to_my_pets
end

Então("o meu acesso é efetuado com sucesso --menu") do
  step "verifico que estou na tela inicial --home"
  step "que o menu inferior é apresentado corretamente para usuários logados --home"
  step "abro o menu --home"
  expecting(menu.is_logged, true, 'Verifica que usuário está logado')
end

E("efetuo o logout") do
  menu.do_logout
  expecting(menu.is_logged, false, 'Verifica que usuário não está logado')
end


Dado("estou logado com sucesso no menu --menu") do |table|
  table.hashes.each do |row|
    step "faço o login com meu email '#{row[:email]}' e senha '#{row[:password]}' --login"
    step 'o meu acesso é efetuado com sucesso --home'
    step 'abro o menu --home'
  end
end

