Entao("verifico se há endereços cadastrados --my-addresses") do
  my_addresses.has_addresses
end

Quando("clico para adicionar um novo endereço --my-addresses") do
  my_addresses.new_address
end

Quando("adiciono um novo endereço {string} --my-addresses") do | location |
  my_addresses.fill_address location
  my_addresses.save_address
end

Então("verifico que o endereço foi adicionado com sucesso --my-addresses") do
  expecting(my_addresses.check_address_added, true, "verifica que a quantidade de endereços foi atualizada")
end

Quando("seleciono um endereço aleatório --my-addresses") do
  my_addresses.select_random_address
  my_addresses.confirm_address
end

