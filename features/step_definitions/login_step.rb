E("vou para a tela de cadastro --login") do
  step 'abro o menu --home'
  step 'escolho a opção de login --menu'
  step 'escolho a opção de cadastro --login'
end



E("escolho a opção de cadastro --login") do
  login.go_to_register
end


Dado("faço o login com meu email {string} e senha {string} --login") do |email, password|
  step "vou para a tela de login --menu"
  login.do_login email, password
end

Então("a mensagem de dados incorretos é apresentada --login") do
  expecting(login.is_invalid_login, true, 'Mensagem de login inválido é apresentada')
end
