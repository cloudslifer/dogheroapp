Dado("estou nos meu heróis com uma localização informada e logado --my-heroes") do | table |
  step("estou na tela inicial com uma localização informada e logado --home", table)
  step "vou para a tela de meus heróis --home"
  step "estou na tela de meus heróis --my-heroes"
end

Então("estou na tela de meus heróis --my-heroes") do
  expecting(my_heroes.is_my_heroes, true, "Verifico que estou na tela de meus heróis")
end

Então("verifico que há heróis favoritados --my-heroes") do
  expecting(my_heroes.check_has_favorite_heroes, true, "Verifica que possui heróis favoritos adicionados")
end

E("vou para o perfil do anfitrião de testes --my-heroes") do | table |
  step("estou nos meu heróis com uma localização informada e logado --my-heroes", table)
  my_heroes.go_to_qa_hero
end

Então("verifico que o herói adicionado na tela de {string} foi adicionado com sucesso --my-heroes") do | page |
  expecting(my_heroes.check_hero_was_added(page), true, "Veririca que o herói foi favoritado com sucesso")
end

E("verifico que as informações são encontradas com sucesso nos cards dos heróis --my-heroes") do
  expecting(my_heroes.check_hero_info, true, "Verifico que os dados no card como 'nome, preço, localização' são apresentados com sucesso")
end

E("removo o herói dos meus favoritos") do
  my_heroes.remove_hero
end

Então("o herói é removido com sucesso") do
  step "abro o menu --home"
  step "vou para a tela de meus heróis --home"
  step "estou na tela de meus heróis --my-heroes"
  expecting(my_heroes.check_hero_was_removed, true, "Verifica que o herói foi removido com sucesso")
end