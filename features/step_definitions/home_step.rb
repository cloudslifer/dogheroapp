Dado("estou na tela inicial com uma localização informada --home") do | table |
  step "clico em continuar na sessão de boas vindas --onboarding"
  step "escolho o idioma default --language"
  step "pulo o onboarding --onboarding"

  table.hashes.each do |row|
    step "escolho um endereço '#{row[:address]}' --location"
  end
  step "verifico que estou na tela inicial --home"
end

Dado("estou na tela inicial com uma localização informada e logado --home") do | table |
  step "clico em continuar na sessão de boas vindas --onboarding"
  step "escolho o idioma default --language"
  step "pulo o onboarding --onboarding"

  table.hashes.each do |row|
    step "escolho um endereço '#{row[:address]}' --location"
    step "verifico que estou na tela inicial --home"
    step "faço o login com meu email '#{row[:email]}' e senha '#{row[:password]}' --login"
  end
  step "verifico que estou na tela inicial --home"
  step "que o menu inferior é apresentado corretamente para usuários logados --home"
end

E("clico para alterar o endereço --home") do
  home.change_address
end

Quando("clico na opção de busca por anfitrião --home") do
  home.go_to_boarding
end
Dado("verifico que estou na tela inicial --home") do
  expecting(home.is_home, true, 'Verifica que está na tela inicial')
end

Entao("verifico os textos apresentados --home") do
  expecting(home.check_home_page, true, 'Valida que a home apresenta os textos corretamente')
end

Entao("que o menu inferior é apresentado corretamente para usuários não logados --home") do
  expecting(home.check_bottom_menu_not_logged, true, 'Valida que o menu para usuários não logados apresenta as opções corretas')
end

Entao("que o menu inferior é apresentado corretamente para usuários logados --home") do
  expecting(home.check_bottom_menu_logged, true, 'Valida que o menu para usuários logados apresenta as opções corretas')
end

E("clico na opção de 'home'") do
  home.back_to_home
end

Quando("vou para a tela de meus heróis --home") do
  tracing 'acessando a tela de heróis'
  home.go_to_my_heroes
end

Quando("abro o menu --home") do
  tracing 'abrindo o menu'
  home.open_menu
end


Então("sou direcionado para a tela inicial com meu endereço preenchido --home") do
  step "verifico que estou na tela inicial --home"
  expecting(home.check_address_is_filled, true, 'Verifica que a localização atual está preenchida')
end

Então("que as funcionalidades principais são apresentadas --home") do
  expecting(home.check_main_features, true, 'Verifica as principais funcionalidades apresentadas na home')
end