**Pré Requisitos:**

1 - Appium Instalado e servidor inicializado de maneira default

2 - DogHero.app na pasta raiz do framework

3 - Informar na classe 'Capabilities' os dados de 'deviceName', 'udid', e 'platformVersion' de acordo com o Simulador desejado 


**Execução fullsmoke:**

_cucumber -p ios -t "@FullSmoke"_


